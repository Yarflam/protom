const { Qureg } = require('../core/quantum');
// const { Struct } = require('../core');

/* Create the quantum system */
const app = new Qureg();
app.addQubit(1, 0);
app.addQubit(1, 0);

/*
 *  Operation
 */
app.H(0)
    .CNot(0, 1)
    // .H(2)

/* Debug */
console.log('Qubits\n' + app.debug(Qureg.DEBUG_QUBITS) + '\n');
console.log('States\n' + app.debug(Qureg.DEBUG_STATES_FULL));
