const { Bayes } = require('../core/bayes');

const sb = (call, enable = true) => {
    if (enable || false) call();
}; // [s]earch-[b]ayes integration function

/* 1 - Basic */
sb(() => {
    const choco = 12; // eat the chocolate
    const student = 17; // student
    const others = 12; // !(chocolate & student)
    const chetu = 7; // chocolate & student
    const pop = choco + student - chetu + others; // population

    let p = {};
    p['B'] = student / pop;
    p['A|B'] = chetu / student;
    p['AnB'] = p['B'] * p['A|B'];
    p['AnB?'] = chetu / pop;

    Object.entries(p).map(([key, value]) => console.log(`${key}\t= ${value}`));
}, false);

/*
 *  Note:
 *      P(x|H) - Credibility of the result x
 *      P(H|x) - Plausibility of the hypothesis H
 *
 *  P(H|x) = (P(x|H) * P(H)) / P(x)
 */

/*
 *  2 - Bayes Theorem
 *      D4, D6, D8, D12, D20
 *      Number 7 drawn.
 *
 *  Apriori: 1 chance out of 5.
 *  Theory : 0, 0, 1/8, 1/12, 1/20
 *  P(Theory:D8|7) = (Theory:D8 * Apriori:D8) / (Theory * Apriori)
 */
sb(() => {
    let apriori = [1 / 5, 1 / 5, 1 / 5, 1 / 5, 1 / 5];
    let theory = [0, 0, 1 / 8, 1 / 12, 1 / 20];

    let p = {};
    p['B|A'] = theory[2]; // the theory (D8 -> 1 out of 8)
    p['A'] = apriori[2]; // the a priori about the theory (hypothesis)
    p['B'] = theory.reduce((accum, t, i) => accum + (t * apriori[i]), 0); // credibility
    p['A|B'] = (p['B|A'] * p['A']) / p['B'];

    Object.entries(p).map(([key, value]) => console.log(`${key}\t= ${value}`));
}, false);

/*
 *  3 - Find the dice (without Machine Learning :p)
 */
sb(() => {
    const dice = [4, 6, 8, 12, 20]; // dice list (labels)
    function rndTable(n, list = dice, rnd = 0) {
        rnd = rnd || (n >= 2 ? Math.floor(Math.random() * n) + 1 : 0); // x or random value
        return list.map(d => (rnd >= 1 && rnd <= d ? 1 / d : 0)); // is it possible?
    }

    /* Prepare a Bayes model */
    let reg = new Bayes();
    new Array(5).fill(1 / 5).map(x => reg.add(x)); // At the beginning, we have 20% for each die
    reg.next().map((x, i) =>
        console.log(`D${dice[i]}:\t${Math.floor(x * 10000) / 100} %`)
    );
    console.log('----- vvv ----- Roll die 7 (1 time)');

    /* Add the number 7 - the register thinks it's a die 8 */
    rndTable(0, dice, 7).map(x => reg.add(x));
    reg.next().map((x, i) =>
        console.log(`D${dice[i]}:\t${Math.floor(x * 10000) / 100} %`)
    );
    console.log('----- vvv ----- Role die 12 (20 times)');

    /* Roll the die 12 - 20 times */
    for (var i = 0; i < 20; i++) {
        rndTable(12).map(x => reg.add(x));
        reg.next();
    }

    /* The system has found the die used (~99.99%) */
    reg.next().map((x, i) =>
        console.log(`D${dice[i]}:\t${Math.floor(x * 10000) / 100} %`)
    );

    /*
     *   If we add the number 19 - surprise! It's the die 20 (~21.5%)
     *      And if we add another number between 13 and 20, the register
     *      thinks it is possible in 99.96%.
     */
    console.log('----- vvv ----- Role die 19 (1 time)');
    rndTable(0, dice, 19).map(x => reg.add(x));
    reg.next().map((x, i) =>
        console.log(`D${dice[i]}:\t${Math.floor(x * 10000) / 100} %`)
    );
}, true);
