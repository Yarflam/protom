const chalk = require('chalk');
const log = console.log;

const world_size = { w: 40, h: 30 };

/* Define the tiles */
// ▓ ᐃ
let tiles = [
    {
        name: 'soil',
        color: '#00AA66',
        ratio: 50
    },
    {
        name: 'water',
        color: '#0066CC',
        ratio: 20
    },
    {
        name: 'mountain',
        color: '#005500',
        ratio: 3
    },
    {
        name: 'cities',
        color: '#F9F9F9',
        ratio: 1
    }
];

/* Create a random shade */
let sum_ratio = tiles.reduce((accum, { ratio }) => accum + ratio, 0);
let shape = tiles
    .map(({ ratio }, index) => ({ index, rand: (1 / sum_ratio) * ratio }))
    .reduce((accum, item) => {
        if (accum.length) {
            item.rand += accum[accum.length - 1].rand;
        }
        return accum.concat([item]);
    }, []);

/* Generate a world map */
let world = new Array(world_size.h).fill(0).map(() =>
    new Array(world_size.w).fill(0).map(() => {
        let rand = Math.random();
        return shape.filter(item => item.rand > rand).shift().index;
    })
);

/* Show the map */
world.map(row => {
    log(
        row
            .map(index => {
                let { color } = tiles[index];
                return chalk.bgHex(color).hex(color)('__');
            })
            .join('')
    );
    return row;
});
