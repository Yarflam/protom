const charlk = require('chalk');
const log = console.log;

/* World dimension */
const dim = { w: 60, h: 60 };

/* Rules */
const rules = {
    flatten: {
        steps: 10,
        delta: 0.25
    }
};

/* Define the tiles */
let tiles = [
    {
        name: 'water',
        color: '#0066CC',
        level: [-1, 0]
    },
    {
        name: 'sand',
        color: '#00FFAA',
        level: [0, 0.08]
    },
    {
        name: 'grass',
        color: '#006611',
        level: [0.08, 0.3]
    },
    {
        name: 'moutain',
        color: '#888888',
        level: [0.3, 0.5]
    },
    {
        name: 'snow',
        color: '#FFF',
        level: [0.5, 1]
    }
];

/* Function Distance */
const distVector = (a, b) => {
    return Math.sqrt(
        (b[0] - a[0]) * (b[0] - a[0]) + (b[1] - a[1]) * (b[1] - a[1])
    );
};

const rand = (a, b) => Math.random() * (b - a) + a;

/* Step 1 - Generate a world (random values between [-1, 1]) */
let dist,
    msquare = Math.max(dim.w, dim.h) / 2;
let world = new Array(dim.h).fill(0).map((_, iRow) =>
    new Array(dim.w).fill(0).map((_, iCol) => {
        /* So far */
        dist = distVector([iCol, iRow], [dim.w / 2, dim.h / 2]);
        if (
            dist > rand(0.4 * msquare, msquare - 1) ||
            dist < rand(0, 0.2 * msquare)
        )
            return rand(-0.5, 0);
        return rand(0, 1) > 0.4 ? rand(0.25, 1) : rand(-0.9, 1);
    })
);

/* Step 2 - Flatten heatmap */
let average,
    iFlat = rules.flatten.steps;
while (iFlat--) {
    world = world.map((row, iRow) =>
        row.map((value, iCol) => {
            /* If it's a border cell, not change */
            if (!iRow || iRow + 1 === dim.h || (!iCol || iCol + 1 === dim.w)) {
                return value;
            }
            /* Calculate an average from 3x3 matrix */
            average =
                [
                    [-1, -1],
                    [0, -1],
                    [1, -1],
                    [-1, 0],
                    [1, 0],
                    [-1, 1],
                    [0, 1],
                    [1, 1]
                ]
                    .map(([x, y]) => world[iRow + y][iCol + x])
                    .reduce((accum, value) => accum + value, 0) / 8;
            /* Apply a transformation */
            return value + (average - value) * rules.flatten.delta;
        })
    );
}

/* Step 3 - Drawing */
world.map(row => {
    log(
        row
            .map(value => {
                let { color } = tiles.filter(
                    ({ level }) => level[0] <= value && value <= level[1]
                )[0];
                return charlk.bgHex(color).hex(color)('__');
            })
            .join('')
    );
    return row;
});
