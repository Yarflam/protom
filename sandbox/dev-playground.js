const { Protom, Struct } = require('../core');
const { Vector } = Struct;

const app = new Protom();

const a = app.var([2, 4], 'a', Protom.Complex);
const b = app.var([3, 3, 3], 'b', Protom.Vector);

console.log(
    a
        .convert(Protom.Vector)
        .add(b)
        .eval()
        .toString()
);

const c = app.var(
    [new Vector([1, 2, 3]), new Vector([4, 5, 6]), new Vector([7, 8, 9])],
    'c',
    Protom.Tensor
);

console.log(
    c
        .convert(Protom.Matrix)
        .eval()
        .toString()
);
