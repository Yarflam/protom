const controllers = require('./struct/controllers');
const datatypes = require('./struct/datatypes');
const nodes = require('./struct/nodes');
const { gentools } = require('./helpers');

/*
 *  Protom
 *      { Main class, v1.0.0 } by Yarflam
 */

class Protom {
    constructor() {
        this._stack = [];
        this._register = [];
    }

    /* Evaluate */
    eval(scope = [], inputs = {}) {
        let item, search, output;

        /* Checking */
        if (
            !(
                Array.isArray(scope) &&
                scope.length &&
                typeof inputs === 'object' &&
                inputs
            )
        ) {
            return [];
        }

        /* Measure */
        let iScope = scope.length;
        output = new Array(iScope);
        for (let i = 0; i < this._stack.length; i++) {
            item = this._stack[i];

            /* Filling */
            if (item instanceof nodes.Placeholder) {
                if (inputs[item.getName()] !== undefined) {
                    item.setValue(inputs[item.getName()]);
                }
            }

            /* Catch the controllers */
            if (
                item instanceof controllers.Operation ||
                item instanceof controllers.Activation ||
                item instanceof controllers.Caller
            ) {
                item.apply();
            }

            /* Remove from the scope */
            search = scope.indexOf(item);
            if (scope.indexOf(item) >= 0) {
                output[search] = item.getValue();
                iScope--;
            }

            /* The scope is complete */
            if (!iScope) {
                break;
            }
        }
        return output;
    }

    /* Register */
    __addRegister(name) {
        if (
            !name ||
            typeof name !== 'string' ||
            this._register.indexOf(name) >= 0 ||
            name.match(/^\$[0-9]+$/) !== null
        ) {
            name = '$' + (this._register.length + 1);
        }
        this._register.push(name);
        return name;
    }

    /* Prepare */
    __addStack(struct, args) {
        let item;
        /* Nodes */
        if (struct === 'const') item = new nodes.Const(this, ...args);
        if (struct === 'var') item = new nodes.Var(this, ...args);
        if (struct === 'placeholder')
            item = new nodes.Placeholder(this, ...args);
        /* Contollers */
        if (struct === 'operation')
            item = new controllers.Operation(this, ...args);
        if (struct === 'activation')
            item = new controllers.Activation(this, ...args);
        if (struct === 'caller') item = new controllers.Caller(this, ...args);
        /* Add it */
        if (!item) return;
        this._stack.push(item);
        return item;
    }

    /* Is it a node ? */
    isNode(obj) {
        return (
            obj !== null &&
            typeof obj === 'object' &&
            (obj instanceof nodes.Const ||
                obj instanceof nodes.Var ||
                obj instanceof nodes.Placeholder ||
                obj instanceof controllers.Operation ||
                obj instanceof controllers.Activation ||
                obj instanceof controllers.Caller)
        );
    }

    /* Is it a datatype ? */
    isDatatype(obj) {
        return (
            obj !== null &&
            typeof obj === 'object' &&
            (obj instanceof datatypes.Num ||
                obj instanceof datatypes.Complex ||
                obj instanceof datatypes.Quaternion ||
                obj instanceof datatypes.Vector ||
                obj instanceof datatypes.Tensor ||
                obj instanceof datatypes.Matrix)
        );
    }

    isDatatypeName(name) {
        return (
            typeof name === 'string' &&
            [
                'Num',
                'Complex',
                'Quaternion',
                'Vector',
                'Tensor',
                'Matrix'
            ].indexOf(name) >= 0
        );
    }

    /* Structures */
    const(...args) {
        return this.__addStack('const', args);
    }
    var(...args) {
        return this.__addStack('var', args);
    }
    placeholder(...args) {
        return this.__addStack('placeholder', args);
    }

    /* Operations */
    add(...args) {
        return this.__addStack('operation', ['add', ...args]);
    }
    sub(...args) {
        return this.__addStack('operation', ['sub', ...args]);
    }
    mul(...args) {
        return this.__addStack('operation', ['mul', ...args]);
    }
    div(...args) {
        return this.__addStack('operation', ['div', ...args]);
    }
    pow(...args) {
        return this.__addStack('operation', ['pow', ...args]);
    }

    /* Activation function */
    gauss(...args) {
        return this.__addStack('activation', ['gauss', ...args]);
    }
    heaviside(...args) {
        return this.__addStack('activation', ['heaviside', ...args]);
    }
    leakyRelu(...args) {
        return this.__addStack('activation', ['leaky_relu', ...args]);
    }
    relu(...args) {
        return this.__addStack('activation', ['relu', ...args]);
    }
    sigmoid(...args) {
        return this.__addStack('activation', ['sigmoid', ...args]);
    }
    logistic(...args) {
        return this.sigmoid(...args);
    }
    swish(...args) {
        return this.__addStack('activation', ['swish', ...args]);
    }
    tanh(...args) {
        return this.__addStack('activation', ['tanh', ...args]);
    }

    /* Helpers */
    zeros(...args) {
        return gentools.zeros(...args);
    }
    rand(...args) {
        return gentools.rand(...args);
    }
    randInt(...args) {
        return gentools.randInt(...args);
    }
    randFloat(...args) {
        return gentools.randFloat(...args);
    }

    /* Converters */
    convert(...args) {
        return this.__addStack('caller', ['convert', ...args]);
    }
    transpose(...args) {
        return this.__addStack('caller', ['transpose', ...args]);
    }
}
Protom.Version = '1.0.0';
Protom.Authors = ['Yarflam'];
Protom.None = null;
Protom.Num = 'Num';
Protom.Complex = 'Complex';
Protom.Quaternion = 'Quaternion';
Protom.Vector = 'Vector';
Protom.Tensor = 'Tensor';
Protom.Matrix = 'Matrix';

/* Sticky Methods */
var stickyTarget = ['Activation', 'Operation', 'Caller', 'Const'],
    stickyMethods = ['add', 'sub', 'mul', 'div', 'pow', 'convert', 'transpose'];
Protom.__stickyMethods = items => {
    stickyTarget.map(name => {
        if (!items[name]) return 0;
        /* Add the methods */
        stickyMethods.map(method => {
            items[name].prototype[method] = function(...args) {
                return this._core[method](this, ...args);
            };
        });
        return 1;
    });
    return items;
};

module.exports = Protom;
