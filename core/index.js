const Protom = require('./Protom');
const Struct = require('./struct');
const bayes = require('./bayes');
const quantum = require('./quantum');

/* One app (for web) */
let Engine = Protom;
Engine.Struct = Struct;
Engine.lab = { bayes, quantum };

/* Auto-linking */
Protom.__stickyMethods(require('./struct'));

/* Export the package */
module.exports = {
    Protom: Engine,
    Struct,
    /* EXPERIMENT LABS */
    ...Engine.lab
};
