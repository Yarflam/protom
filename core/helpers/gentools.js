const gentools = {
    /*
     *   Generate a tensor
     */
    _buildTensor: (dim, builder) => {
        if (typeof dim === 'number') dim = [dim];
        if (!Array.isArray(dim)) return [];
        /* Is ready to generate */
        let i,
            output = [],
            readers = [output];
        for (i = 0; i < dim.length; i++) {
            if (i == dim.length - 1) {
                /* Generate the numbers */
                readers.map(reader => {
                    let subs = new Array(dim[i]).fill(0);
                    subs = builder || false ? subs.map(builder) : subs;
                    reader.splice(0, 0, ...subs);
                    return reader;
                });
            } else {
                /* Next step */
                readers = readers
                    .map(reader => {
                        let subs = new Array(dim[i]).fill(0).map(() => []);
                        reader.splice(0, 0, ...subs);
                        return subs;
                    })
                    .reduce((accum, reader) => accum.concat(reader), []);
            }
        }
        return output;
    },
    /* Variant with many zeros */
    zeros: dim => gentools._buildTensor(dim),
    /* Variant with random numbers */
    rand: dim => gentools._buildTensor(dim, () => Math.random()),
    /* Variant with integers */
    randInt: (dim, a, b) => {
        if (typeof a !== 'number' || typeof b !== 'number') return [];
        if (b < a) [a, b] = [b, a];
        return gentools._buildTensor(dim, () =>
            Math.floor(Math.random() * (b - a + 1) + a)
        );
    },
    /* Variant with floats */
    randFloat: (dim, a, b) => {
        if (typeof a !== 'number' || typeof b !== 'number') return [];
        if (b < a) [a, b] = [b, a];
        return gentools._buildTensor(dim, () => Math.random() * (b - a) + a);
    }
};

module.exports = gentools;
