module.exports = {
    /* Datatypes */
    ...require('./datatypes'),
    /* Nodes */
    ...require('./nodes'),
    /* Controllers */
    ...require('./controllers')
};
