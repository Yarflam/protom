/*
 *  Num - class @types
 *  A simple Number.
 */

class Num {
    constructor(value = 0) {
        this.setValue(value);
    }

    toString() {
        return `${this._value}`;
    }

    /*
     *  Operations
     */

    __ope(obj, sign, inv = false) {
        let result = null;
        /* Less complex object */
        if (typeof obj === 'number') obj = new Num(obj);
        /* More complex object */
        if (!(obj instanceof Num)) {
            return typeof obj === 'object' && typeof obj.__ope === 'function'
                ? obj.__ope(this, sign, true)
                : new Num();
        }
        /* Applying the calculation */
        let [a, b] = !inv ? [this, obj] : [obj, this];
        if(sign === Num.OPE_ADD) result = a._value + b._value;
        if(sign === Num.OPE_SUB) result = a._value - b._value;
        if(sign === Num.OPE_MUL) result = a._value * b._value;
        if(sign === Num.OPE_DIV) result = a._value / b._value;
        if(sign === Num.OPE_POW) result = Math.pow(a._value, b._value);
        if(sign === Num.OPE_MOD) {
            while (a._value < b._value) a._value += b._value;
            result = a._value % b._value;
        }
        if(result === null) result = a._value;
        return new Num(result);
    }

    add(obj) {
        return this.__ope(obj, Num.OPE_ADD);
    }
    sub(obj) {
        return this.__ope(obj, Num.OPE_SUB);
    }
    mul(obj) {
        return this.__ope(obj, Num.OPE_MUL);
    }
    div(obj) {
        return this.__ope(obj, Num.OPE_DIV);
    }
    pow(obj) {
        return this.__ope(obj, Num.OPE_POW);
    }
    mod(obj) {
        return this.__ope(obj, Num.OPE_MOD);
    }

    pow2() {
        return this.__ope(this, Num.OPE_MUL);
    }

    inv() {
        return this.__ope(-1, Num.OPE_MUL);
    }

    /* Math */

    abs() {
        return new Num(Math.abs(this._value));
    }

    sqrt() {
        return new Num(Math.sqrt(this._value));
    }

    cos() {
        return new Num(Math.cos(this._value));
    }

    sin() {
        return new Num(Math.sin(this._value));
    }

    tan() {
        return new Num(Math.tan(this._value));
    }

    log() {
        return new Num(Math.log(this._value));
    }

    exp() {
        return new Num(Math.exp(this._value));
    }

    /* Logical gates (draft) */

    AND(obj) {
        return new Num(this.isNotZero() && obj.isNotZero() ? 1 : 0);
    }

    OR(obj) {
        return new Num(this.isNotZero() || obj.isNotZero() ? 1 : 0);
    }

    NOT() {
        return new Num(!this.isNotZero() ? 1 : 0);
    }

    NOR(obj) {
        return new Num(!(this.isNotZero() || obj.isNotZero()) ? 1 : 0);
    }

    NAND(obj) {
        return new Num(!(this.isNotZero() && obj.isNotZero()) ? 1 : 0);
    }

    XOR(obj) {
        return new Num(
            (this.isNotZero() || obj.isNotZero()) &&
            !(this.isNotZero() && obj.isNotZero())
                ? 1
                : 0
        );
    }

    XNOR(obj) {
        return new Num(
            !(
                (this.isNotZero() || obj.isNotZero()) &&
                !(this.isNotZero() && obj.isNotZero())
            )
                ? 1
                : 0
        );
    }

    /* Conditions */

    isNotZero() {
        return this._value !== 0;
    }

    /* Conversions */

    convert(type) {
        if (type === Num.CONVERT_COMPLEX) return this.toComplex();
        if (type === Num.CONVERT_QUATERNION) return this.toQuaternion();
        if (type === Num.CONVERT_VECTOR) return this.toVector();
        if (type === Num.CONVERT_MATRIX) return this.toMatrix();
        if (type === Num.CONVERT_TENSOR) return this.toTensor();
        return this;
    }

    toComplex() {
        const Complex = require('./Complex');
        return new Complex(this._value);
    }

    toQuaternion() {
        const Quaternion = require('./Quaternion');
        return new Quaternion(0, this._value);
    }

    toVector() {
        const Vector = require('./Vector');
        return new Vector([this._value]);
    }

    toMatrix() {
        const Matrix = require('./Matrix');
        return new Matrix([this._value]);
    }

    toTensor() {
        const Tensor = require('./Tensor');
        return new Tensor([this.toVector()]);
    }

    /*
     *  GETTERS
     */

    getValue() {
        return this._value;
    }

    /*
     *  SETTERS
     */

    setValue(value) {
        this._value = Number(value);
        if (isNaN(this._value)) this._value = 0;
        this._value = Math.round(this._value * Num.PRECISION) / Num.PRECISION; // precision
        return this;
    }
}
Num.OPE_ADD = 'add';
Num.OPE_SUB = 'sub';
Num.OPE_MUL = 'mul';
Num.OPE_DIV = 'div';
Num.OPE_POW = 'pow';
Num.OPE_MOD = 'mod';
Num.CONVERT_COMPLEX = 'Complex';
Num.CONVERT_QUATERNION = 'Quaternion';
Num.CONVERT_VECTOR = 'Vector';
Num.CONVERT_MATRIX = 'Matrix';
Num.CONVERT_TENSOR = 'Tensor';
Num.PRECISION = 1e6;

module.exports = Num;
