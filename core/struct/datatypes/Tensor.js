/*
 *  Tensor - class @types
 *  A space multidimensional.
 */

class Tensor {
    /*
     *  Constructor
     *  @args:
     *      value:Array<Any>
     */
    constructor(value = []) {
        this.setValue(value);
    }

    toString (leftSpace='  ') {
        if(!this._value.length) return '[]';
        let out = '';
        let lastLevel = 0;
        let parts = [[0, this._value]];
        for(let i=0; i < parts.length; i++) {
            const [ level, items ] = parts[i];
            const leftPad = new Array(level).fill(leftSpace).join('');
            /* Down */
            if(i && level === lastLevel) out += ',\n';
            if(level < lastLevel) {
                out += '\n' + new Array(lastLevel-level).fill(0).map(
                    (_, index) => `${new Array(lastLevel-index-1).fill(leftSpace).join('')}],`
                ).join('\n') + '\n';
            }
            /* Analyze */
            const hasLeaf = items.some(child => !Array.isArray(child));
            if(hasLeaf || !items.length) {
                out += `${leftPad}[ ${items.join(', ')} ]`;
            } else {
                out += `${leftPad}[\n`;
                parts.splice(i+1, 0, ...items.map(child => [ level+1, child ]));
            }
            lastLevel = level;
        }
        /* Close all brackets */
        if(lastLevel) out += '\n';
        while(lastLevel--) {
            const leftPad = new Array(lastLevel).fill(leftSpace).join('');
            out += `${leftPad}]${lastLevel ? '\n': ''}`;
        }
        return out;
    }

    shape () {
        //
    }

    reshape () {
        //
    }

    /*
     *   Operations
     */

    __walkChange(fct, item=this.getValue()) {
        if(typeof fct !== 'function' || !Array.isArray(item)) return [];
        let parts = [item];
        for(let j, i=0; i < parts.length; i++) {
            const hasLeaf = parts[i].some(child => !Array.isArray(child));
            if(hasLeaf) {
                for(j=0; j < parts[i].length; j++) {
                    if(Array.isArray(parts[i][j])) parts[i][j] = 0;
                    parts[i][j] = fct(parts[i][j]); // by reference
                }
            } else parts.push(...parts[i]);
        }
        return item;
    }

    __ope(obj, sign, inv = false) {
        let result;
        const { Num, Complex, Quaternion, Vector, Matrix } = require('./index');
        /* Less complex object */
        if(typeof obj === 'number') obj = new Num(obj);
        if (obj instanceof Vector || obj instanceof Matrix)
            obj = new Tensor(obj);
        const isNumber = obj instanceof Num || obj instanceof Complex ||
            obj instanceof Quaternion;
        /* More complex object (extensions) */
        if (!(obj instanceof Tensor || isNumber)) {
            return typeof obj === 'object' && typeof obj.__ope === 'function'
                ? obj.__ope(this, sign, true)
                : new Tensor();
        }
        /* Switch */
        let [a, b] = !inv ? [this, obj] : [obj, this];
        if(isNumber) {
            result = [[]];
            let parts = [a._value];
            for(let j, i=0; i < parts.length; i++) {
                const hasLeaf = parts[i].some(child => !Array.isArray(child));
                if(!hasLeaf) {
                    parts.push(...parts[i]);
                    result[i].push(...new Array(parts[i].length).fill(0).map(() => []));
                    result.push(...result[i]);
                } else if(b.isNotZero()) {
                    for(j=0; j < parts[i].length; j++) {
                        result[i].push(parts[i][j].mul(b));
                    }
                } else {
                    result[i].push(...new Array(parts[i].length).fill(0).map(x => new Num(x)));
                }
            }
            return new Tensor(result[0]);
        }
        /* Multi-walking */
        result = [[]];
        let leftPart = [a._value], rightPart = [b._value];
        for(let j, i=0; i < leftPart.length; i++) {
            const leftLeaft = leftPart[i].some(child => !Array.isArray(child));
            const rightLeaft = rightPart[i].some(child => !Array.isArray(child));
            if(
                !Tensor.OPE_PRODUCT &&
                (leftPart[i].length !== rightPart[i].length || leftLeaft !== rightLeaft)
            ) {
                result[0] = []; 
                break;
            }
            /* Children */
            if(
                !leftLeaft && !rightLeaft
            ) {
                leftPart.push(...leftPart[i]);
                rightPart.push(...rightPart[i]);
                result[i].push(...new Array(leftPart[i].length).fill(0).map(() => []));
                result.push(...result[i]);
                continue;
            } else if(sign === Tensor.OPE_PRODUCT) {
                for(j=0; j < leftPart[i].length; j++) {
                    result[i].push(...new Tensor(rightPart[0]).mul(leftPart[i][j]).getValue());
                }
                if(!Array.isArray(result[i][0])) continue;
                /* Move it */
                let square = [];
                for(j=0; j < result[i].length; j++) {
                    if(!square[j % rightPart[i].length]) square.push([]);
                    square[j % rightPart[i].length].push(...(
                        Array.isArray(result[i][j]) ? result[i][j] : [result[i][j]]
                    ));
                }
                const parent = result.find(slot => slot.indexOf(result[i]) >= 0);
                if(parent) {
                    parent.splice(parent.indexOf(result[i]), 1, ...square);
                } else result[0] = square;
                continue;
            } else {
                for(j=0; j < leftPart[i].length; j++) {
                    result[i].push(
                        this.__opeUnit(sign, leftPart[i][j], rightPart[i][j])
                    );
                }
            }
        }
        return new Tensor(result[0]);
    }
    __opeUnit (sign, a, b) {
        if(Array.isArray(a)) return a.map(x => this.__opeUnit(sign, x, b));
        if(sign === Tensor.OPE_ADD) return a.add(b);
        if(sign === Tensor.OPE_SUB) return a.sub(b);
        if(sign === Tensor.OPE_MUL) return a.mul(b);
        if(sign === Tensor.OPE_DIV) return a.div(b);
        /* Default value */
        const Num = require('./Num');
        return new Num(0);
    }

    add(obj) {
        return this.__ope(obj, Tensor.OPE_ADD);
    }
    sub(obj) {
        return this.__ope(obj, Tensor.OPE_SUB);
    }
    mul(obj) {
        return this.__ope(obj, Tensor.OPE_MUL);
    }
    div(obj) {
        return this.__ope(obj, Tensor.OPE_DIV);
    }

    product(obj=this, n=2) {
        if(typeof n !== 'number' || !n) return new Tensor();
        if(n <= 1) return obj;
        /* 2x */
        n = Math.ceil(n);
        if(n === 2) return this.__ope(obj, Tensor.OPE_PRODUCT);
        /* Scale up */
        while(--n) obj = this.__ope(obj, Tensor.OPE_PRODUCT);
        return obj;
    }

    /* Conversions */

    convert(type) {
        if (type === Tensor.CONVERT_VECTOR) return this.toVector();
        if (type === Tensor.CONVERT_MATRIX) return this.toMatrix();
        return this;
    }

    toVector() {
        const Vector = require('./Vector');
        return new Vector(this._value);
    }

    toMatrix() {
        const Matrix = require('./Matrix');
        return new Matrix(this._value); //.transpose();
    }

    /*
     *  GETTERS
     */

    getValue() {
        return this._value;
    }

    getShape() {
        return this._shape;
    }

    /*
     *  SETTERS
     */

    setValue(value) {
        const { Num, Complex, Quaternion, Vector, Matrix } = require('./index');
        /* Security */
        if(value instanceof Vector) value = value.getValue();
        if(value instanceof Matrix) value = value.getValue();
        if(!Array.isArray(value)) value = [value];
        /* Normalization */
        this._value = this.__walkChange((item) => {
            if(typeof item === 'number') return new Num(item);
            if(
                item instanceof Num ||
                item instanceof Complex ||
                item instanceof Quaternion ||
                item instanceof Vector ||
                item instanceof Matrix
            ) {
                return item;
            }
            return new Num(0);
        }, value);
        return this;
    }
}
Tensor.OPE_ADD = 'add';
Tensor.OPE_SUB = 'sub';
Tensor.OPE_MUL = 'mul';
Tensor.OPE_DIV = 'div';
Tensor.OPE_PRODUCT = 'product';
Tensor.CONVERT_VECTOR = 'Vector';
Tensor.CONVERT_MATRIX = 'Matrix';

module.exports = Tensor;
