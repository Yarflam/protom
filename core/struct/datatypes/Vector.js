/*
 *  Vector - class @types
 *  A space with one dimension
 */

class Vector {
    /*
     *  Constructor
     *  @args:
     *      value:Array<Any>
     */
    constructor(value = [], ...flat) {
        this.setValue([].concat(value).concat(flat));
    }

    toString() {
        return `<${this._value
            .map(x => x.toString())
            .join(', ')
            .replace(/ +/g, ' ')
            .replace(/(\t|\n)/g, '') || 'EMPTY'}>`;
    }

    /*
     *  Operations
     */

    __ope(obj, sign, inv = false) {
        let result;
        const { Num, Complex, Quaternion } = require('./index');
        /* Less complex object */
        if (
            typeof obj === 'number' ||
            obj instanceof Num ||
            obj instanceof Complex ||
            obj instanceof Quaternion
        )
            obj = new Vector(obj);
        /* More complex object */
        if (!(obj instanceof Vector)) {
            return typeof obj === 'object' && typeof obj.__ope === 'function'
                ? obj.__ope(this, sign, true)
                : new Vector();
        }
        /* Balance */
        let [a, b] = !inv ? [this, obj] : [obj, this];
        a._value = a._value.concat(
            new Array(Math.max(0, b.getLength() - a.getLength()))
                .fill(a.getLength() == 1 ? a._value[0] : 0)
                .map(x => new Num(x))
        );
        b._value = b._value.concat(
            new Array(Math.max(0, a.getLength() - b.getLength()))
                .fill(b.getLength() == 1 ? b._value[0] : 0)
                .map(x => new Num(x))
        );
        result = new Array(a.getLength()).fill(0);
        /* Applying the calculation */
        if(sign === Vector.OPE_ADD) result = result.map((_, i) => a._value[i].add(b._value[i]));
        if(sign === Vector.OPE_SUB) result = result.map((_, i) => a._value[i].sub(b._value[i]));
        if(sign === Vector.OPE_MUL) result = result.map((_, i) => a._value[i].mul(b._value[i]));
        if(sign === Vector.OPE_DIV) result = result.map((_, i) => a._value[i].div(b._value[i]));
        if(sign === Vector.OPE_NORM) {
            return result
                .reduce((accum, _, i) => accum.add(
                    b._value[i].sub(a._value[i]).pow2()
                ), new Complex(0))
                .sqrt();
        }
        if(!result) result = this._value;
        return new Vector(result);
    }

    add(obj) {
        return this.__ope(obj, Vector.OPE_ADD);
    }
    sub(obj) {
        return this.__ope(obj, Vector.OPE_SUB);
    }
    mul(obj) {
        return this.__ope(obj, Vector.OPE_MUL);
    }
    div(obj) {
        return this.__ope(obj, Vector.OPE_DIV);
    }
    norm(obj) {
        /* Two vectors */
        if (obj || false) {
            return this.__ope(obj, Vector.OPE_NORM);
        }
        /* One vector */
        const Complex = require('./Complex');
        return this._value
            .map((_, i) => this._value[i].pow2())
            .reduce((accum, x) => accum.add(x), new Complex(0))
            .sqrt();
    }

    /* Math */

    sqrt() {
        return this._value.map(x => x.sqrt());
    }

    normalize() {
        let norm = this.norm();
        if (!norm.isNotZero()) norm.setValue(1);
        return new Vector(this._value.map(x => x.div(norm)));
    }

    /* Conversions */

    convert(type) {
        if (type === Vector.CONVERT_MATRIX) return this.toMatrix();
        if (type === Vector.CONVERT_TENSOR) return this.toTensor();
        return this;
    }

    toMatrix(mode = Vector.MATRIX_DENSE) {
        const Matrix = require('./Matrix');
        /* Conversion mode */
        if (mode === Vector.MATRIX_FLAT) {
            return new Matrix(this._value);
        } else {
            const vLen = this.getLength();
            return new Matrix(
                this._value
                    .reduce(
                        (accum, x, i) =>
                            accum.concat([
                                new Array(vLen)
                                    .fill(0)
                                    .map((_, j) => (i === j ? x : 0))
                            ]),
                        []
                    )
            );
        }
    }

    toTensor() {
        const Tensor = require('./Tensor');
        return new Tensor([this]);
    }

    /*
     *  GETTERS
     */

    getValue() {
        return this._value;
    }

    getLength() {
        return this._value.length;
    }

    /*
     *  SETTERS
     */

    setValue(value, replace = true) {
        const { Num } = require('./index');
        /* Normalization */
        const entries = (Array.isArray(value) ? value : [value])
            .filter(x => x && (typeof x === 'number' || typeof x === 'object'))
            .map(x => (typeof x === 'number' ? new Num(x) : x));
        if (replace) {
            this._value = entries;
        } else this._value.push(...entries);
        return this;
    }

    push(value) {
        this.setValue(value, false);
        return this;
    }
}
Vector.OPE_ADD = 'add';
Vector.OPE_SUB = 'sub';
Vector.OPE_MUL = 'mul';
Vector.OPE_DIV = 'div';
Vector.OPE_NORM = 'norm';
Vector.CONVERT_MATRIX = 'Matrix';
Vector.CONVERT_TENSOR = 'Tensor';
Vector.MATRIX_FLAT = 'flat';
Vector.MATRIX_DENSE = 'dense';

module.exports = Vector;
