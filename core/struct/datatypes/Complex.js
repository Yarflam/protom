/*
 *  Complex - class @types
 *  A complex number (real + imaginary).
 */

class Complex {
    constructor(real = 0, imaginary = 0) {
        this.setValue(real, imaginary);
    }

    toString() {
        const [r, i] = [this._r.getValue(), this._i.getValue()];
        if (!r && !i) return '0';
        return r && i
            ? i > 0
                ? `${r}+${i}i`
                : `${r}-${Math.abs(i)}i`
            : r
                ? `${r}`
                : `${i}i`;
    }

    /*
     *  Operations
     */

    __ope(obj, sign, inv = false) {
        let result;
        const Num = require('./Num');
        /* Less complex object */
        if (
            typeof obj === 'number' ||
            Array.isArray(obj) ||
            obj instanceof Num
        ) {
            obj = new Complex(obj);
        }
        /* More complex object */
        if (!(obj instanceof Complex)) {
            return typeof obj === 'object' && typeof obj.__ope === 'function'
                ? obj.__ope(this, sign, true)
                : new Complex();
        }
        /* Applying the calculation */
        let tmp = {};
        let [a, b] = !inv ? [this, obj] : [obj, this];
        if(sign === Complex.OPE_ADD) {
            result = [a._r.add(b._r), a._i.add(b._i)];
        }
        if(sign === Complex.OPE_SUB) {
            result = [a._r.sub(b._r), a._i.sub(b._i)];
        }
        if(sign === Complex.OPE_MUL) {
            result = [
                a._r.mul(b._r).sub(a._i.mul(b._i)),
                a._r.mul(b._i).add(a._i.mul(b._r))
            ];
        }
        if(sign === Complex.OPE_DIV) {
            result = [
                a._r
                    .mul(b._r)
                    .add(a._i.mul(b._i))
                    .div(b._r.mul(b._r).add(b._i.mul(b._i))),
                a._i
                    .mul(b._r)
                    .sub(a._r.mul(b._i))
                    .div(b._r.mul(b._r).add(b._i.mul(b._i)))
            ];
        }
        if(sign === Complex.OPE_POW) {
            if (b._i.isNotZero()) {
                result = [
                    a._r.pow(b._r).mul(b._i.mul(b._r.log()).cos()),
                    a._r.pow(b._r).mul(b._i.mul(b._r.log()).sin())
                ];
            } else if (b._r.isNotZero()) {
                tmp.x = [a._r, a._i];
                tmp.m = Math.abs(b._r.getValue()) - 1;
                result = [...tmp.x];
                while (tmp.m--) {
                    result = [
                        result[0]
                            .mul(result[0])
                            .sub(result[1].mul(result[1])),
                        result[0]
                            .mul(result[1])
                            .add(result[1].mul(result[0]))
                    ];
                }
            } else result = [1, 0];
        }
        if(sign === Complex.OPE_NORM) {
            return b._r
                .sub(a._r)
                .pow2()
                .add(b._i.sub(a._i).pow2())
                .sqrt();
        }
        if(!result) result = [a._r, a._i];
        return new Complex(...result);
    }

    add(obj) {
        return this.__ope(obj, Complex.OPE_ADD);
    }
    sub(obj) {
        return this.__ope(obj, Complex.OPE_SUB);
    }
    mul(obj) {
        return this.__ope(obj, Complex.OPE_MUL);
    }
    div(obj) {
        return this.__ope(obj, Complex.OPE_DIV);
    }
    pow(obj) {
        return this.__ope(obj, Complex.OPE_POW);
    }
    norm(obj = 0) {
        return this.__ope(obj, Complex.OPE_POW);
    }

    pow2() {
        return this.__ope(this, Complex.OPE_MUL);
    }

    inv() {
        return this.__ope(-1, Complex.OPE_MUL);
    }

    /* Math */

    abs() {
        return new Complex(this._r.abs(), this._i.abs());
    }

    sqrt() {
        return this._r.add(this._i).sqrt();
    }

    exp() {
        return new Complex(
            this._i.cos(), this._i.sin()
        ).mul(this._r.exp());
    }

    /* Conditions */

    isNotZero() {
        return this._r.isNotZero() || this._i.isNotZero();
    }

    /* Conversions */

    convert(type) {
        if (type === Complex.CONVERT_QUATERNION) return this.toQuaternion();
        if (type === Complex.CONVERT_VECTOR) return this.toVector();
        if (type === Complex.CONVERT_MATRIX) return this.toMatrix();
        if (type === Complex.CONVERT_TENSOR) return this.toTensor();
        return this;
    }

    toQuaternion() {
        const Quaternion = require('./Quaternion');
        return new Quaternion(0, this._r, this._i);
    }

    toVector() {
        const Vector = require('./Vector');
        return new Vector([this._r, this._i]);
    }

    toMatrix() {
        const Matrix = require('./Matrix');
        return new Matrix([
            [this._r, 0],
            [0, this._i]
        ]);
    }

    toTensor() {
        const Tensor = require('./Tensor');
        return new Tensor([this.toVector()]);
    }

    /*
     *  GETTERS
     */

    getValue() {
        return [this._r, this._i];
    }

    getReal() {
        return this._r;
    }

    getImaginary() {
        return this._i;
    }

    /*
     *  SETTERS
     */

    setReal(real = 0) {
        const Num = require('./Num');
        this._r = real instanceof Num ? real : new Num(real);
        return this;
    }

    setImaginary(imaginary = 0) {
        const Num = require('./Num');
        this._i = imaginary instanceof Num ? imaginary : new Num(imaginary);
        return this;
    }

    setValue(real = 0, imaginary = 0) {
        if (Array.isArray(real)) {
            [real, imaginary] = [...real, 0, 0];
        }
        return this.setReal(real).setImaginary(imaginary);
    }
}
Complex.OPE_ADD = 'add';
Complex.OPE_SUB = 'sub';
Complex.OPE_MUL = 'mul';
Complex.OPE_DIV = 'div';
Complex.OPE_POW = 'pow';
Complex.OPE_NORM = 'norm';
Complex.CONVERT_QUATERNION = 'Quaternion';
Complex.CONVERT_VECTOR = 'Vector';
Complex.CONVERT_MATRIX = 'Matrix';
Complex.CONVERT_TENSOR = 'Tensor';

module.exports = Complex;
