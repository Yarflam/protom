/*
 *  Quaternion - class @types
 *  A quaternion number
 */

class Quaternion {
    /*
     *  Constructor
     *  @args:
     *      value:Array<Any>
     */
    constructor(w = 0, i = 0, j = 0, k = 0) {
        this.setValue(w, i, j, k);
    }

    toString() {
        return ['w', 'i', 'j', 'k'].reduce((accum, key) => {
            let x = this[`_${key}`].getValue();
            return `${accum}${
                x >= 0 ? (accum.length ? '+' : '') : '-'
            }${Math.abs(x)}${key != 'w' ? key : ''}`;
        }, '');
    }

    /*
     *  Operations
     */

    __ope(obj, sign, inv = false) {
        let result, tmp;
        const { Num, Complex } = require('./index');
        /* Less complex object */
        if (
            typeof obj === 'number' ||
            Array.isArray(obj) ||
            obj instanceof Num
        ) {
            obj = new Quaternion(obj);
        }
        /* Complex number (special) */
        if (obj instanceof Complex) obj = new Quaternion(0, obj._r, obj._i);
        /* More complex object */
        if (!(obj instanceof Quaternion)) {
            return typeof obj === 'object' && typeof obj.__ope === 'function'
                ? obj.__ope(this, sign, true)
                : new Quaternion();
        }
        /* Applying the calculation */
        let [a, b] = !inv ? [this, obj] : [obj, this];
        if(sign === Quaternion.OPE_ADD) {
            result = [
                a._w.add(b._w),
                a._i.add(b._i),
                a._j.add(b._j),
                a._k.add(b._k)
            ];
        }
        if(sign === Quaternion.OPE_SUB) {
            result = [
                a._w.sub(b._w),
                a._i.sub(b._i),
                a._j.sub(b._j),
                a._k.sub(b._k)
            ];
        }
        if(sign === Quaternion.OPE_MUL) {
            result = [
                a._w
                    .mul(b._w)
                    .sub(a._i.mul(b._i))
                    .sub(a._j.mul(b._j))
                    .sub(a._k.mul(b._k)),
                a._i
                    .mul(b._w)
                    .add(a._w.mul(b._i))
                    .add(a._k.mul(b._j))
                    .sub(a._j.mul(b._k)),
                a._j
                    .mul(b._w)
                    .sub(a._k.mul(b._i))
                    .add(a._w.mul(b._j))
                    .add(a._i.mul(b._k)),
                a._k
                    .mul(b._w)
                    .add(a._j.mul(b._i))
                    .sub(a._i.mul(b._j))
                    .add(a._w.mul(b._k))
            ];
        }
        if(sign === Quaternion.OPE_DIV) {
            tmp = {
                sqrt: b._w
                    .mul(b._w)
                    .add(b._i.mul(b._i))
                    .add(b._j.mul(b._j))
                    .add(b._k.mul(b._k))
            };
            result = [
                a._w
                    .mul(b._w)
                    .add(a._i.mul(b._i))
                    .add(a._j.mul(b._j))
                    .add(a._k.mul(b._k))
                    .div(tmp.sqrt),
                a._i
                    .mul(b._w)
                    .sub(a._w.mul(b._i))
                    .sub(a._k.mul(b._j))
                    .add(a._j.mul(b._k))
                    .div(tmp.sqrt),
                a._j
                    .mul(b._w)
                    .add(a._k.mul(b._i))
                    .sub(a._w.mul(b._j))
                    .sub(a._i.mul(b._k))
                    .div(tmp.sqrt),
                a._k
                    .mul(b._w)
                    .sub(a._j.mul(b._i))
                    .add(a._i.mul(b._j))
                    .sub(a._w.mul(b._k))
                    .div(tmp.sqrt)
            ];
        }
        if(sign === Quaternion.OPE_NORM) {
            return ['i', 'j', 'k']
                .map(key => b[`_${key}`].sub(a[`_${key}`]).pow2())
                .reduce((accum, x) => accum.add(x), new Num(0))
                .sqrt();
        }
        /* Default */
        if(!result) result = [a._w, a._i, a._j, a._k];
        return new Quaternion(...result);
    }

    add(obj) {
        return this.__ope(obj, Quaternion.OPE_ADD);
    }
    sub(obj) {
        return this.__ope(obj, Quaternion.OPE_SUB);
    }
    mul(obj) {
        return this.__ope(obj, Quaternion.OPE_MUL);
    }
    div(obj) {
        return this.__ope(obj, Quaternion.OPE_DIV);
    }
    norm(obj = 0) {
        return this.__ope(obj, Quaternion.OPE_NORM);
    }

    /* Math */

    abs() {
        return new Quaternion(this.getValue().map(x => x.abs()));
    }

    neg() {
        return new Quaternion(this.getValue().map(x => x.inv()));
    }

    conjugate() {
        return new Quaternion(this.getValue().map((x, i) => (i ? x.inv() : x)));
    }

    normalize() {
        let norm = this.norm();
        return new Quaternion(
            0,
            ...['i', 'j', 'k'].map(key => this[`_${key}`].div(norm))
        );
    }

    /* Conditions */

    isNotZero() {
        return this.getValue().reduce(
            (accum, n) => accum && n.isNotZero(),
            true
        );
    }

    /* Conversions */

    convert(type) {
        if (type === Quaternion.CONVERT_VECTOR) return this.toVector();
        if (type === Quaternion.CONVERT_MATRIX) return this.toMatrix();
        if (type === Quaternion.CONVERT_TENSOR) return this.toTensor();
        return this;
    }

    toVector() {
        const Vector = require('./Vector');
        return new Vector([this._i, this._j, this._k]);
    }

    toMatrix() {
        const Matrix = require('./Matrix');
        return new Matrix([[this._i, 0, 0], [0, this._j, 0], [0, 0, this._k]]);
    }

    toTensor() {
        const Tensor = require('./Tensor');
        return new Tensor([this.toVector()]);
    }

    /*
     *  GETTERS
     */

    getValue() {
        return [this._w, this._i, this._j, this._k];
    }

    getXYZ(useObj = false) {
        return useObj
            ? {
                x: this._i,
                y: this._j,
                z: this._k
            }
            : [this._i, this._j, this._k];
    }

    /*
     *  SETTERS
     */

    setW(value) {
        const Num = require('./Num');
        this._w = value instanceof Num ? value : new Num(value);
        return this;
    }

    setIJK(i = 0, j = 0, k = 0) {
        const Num = require('./Num');
        this._i = i instanceof Num ? i : new Num(i);
        this._j = j instanceof Num ? j : new Num(j);
        this._k = k instanceof Num ? k : new Num(k);
        return this;
    }

    setXYZ(x = 0, y = 0, z = 0) {
        return this.setIJK(x, y, z); // alias
    }

    setValue(w = 0, i = 0, j = 0, k = 0) {
        if (Array.isArray(w)) {
            [w, i, j, k] = [...w, 0, 0, 0, 0];
        }
        return this.setW(w).setIJK(i, j, k);
    }
}
Quaternion.OPE_ADD = 'add';
Quaternion.OPE_SUB = 'sub';
Quaternion.OPE_MUL = 'mul';
Quaternion.OPE_DIV = 'div';
Quaternion.OPE_NORM = 'norm';
Quaternion.CONVERT_VECTOR = 'Vector';
Quaternion.CONVERT_MATRIX = 'Matrix';
Quaternion.CONVERT_TENSOR = 'Tensor';

module.exports = Quaternion;
