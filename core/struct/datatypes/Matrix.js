/*
 *  Matrix - class @types
 *  A space with two dimensions
 */

class Matrix {
    /*
     *  Constructor
     *  @args:
     *      value:Array<Any>
     */
    constructor(value = []) {
        this.setValue(value);
    }

    toString() {
        /* Max margin between each number */
        let margin = this._value.reduce(
            (accum, row) =>
                Math.max(
                    accum,
                    row.reduce(
                        (accum, col) =>
                            Math.max(
                                accum,
                                Math.floor(
                                    Math.log10(
                                        Math.max(1, col.toString().length)
                                    )
                                )
                            ),
                        0
                    )
                ),
            0
        );
        /* Calculate the output */
        return `[\n${this._value
            .map(row => {
                return `  [ ${row
                    .map(col => {
                        return (
                            new Array(
                                margin -
                                    Math.floor(
                                        Math.log10(
                                            Math.max(1, col.toString().length)
                                        )
                                    )
                            )
                                .fill(' ')
                                .join('') + col.toString()
                        );
                    })
                    .join(', ')} ],`;
            })

            .join('\n')
            .slice(0, -1)}\n]`;
    }

    /*
     *  Operations
     */

    __ope(obj, sign, inv = false) {
        let result = [];
        const { Num, Complex, Quaternion, Vector } = require('./index');
        /* Less complex object */
        if (typeof obj === 'number') obj = new Num(obj);
        if (obj instanceof Vector) obj = new Matrix(obj);
        /* More complex object */
        if (
            !(
                obj instanceof Matrix ||
                obj instanceof Num ||
                obj instanceof Complex ||
                obj instanceof Quaternion
            )
        ) {
            return typeof obj === 'object' && typeof obj.__ope === 'function'
                ? obj.__ope(this, sign, true)
                : new Matrix();
        }
        /* Applying the calculation */
        let i, j, k, tmp;
        let [a, b] =
            !inv || !(obj instanceof Matrix) ? [this, obj] : [obj, this];
        if (b instanceof Matrix) {
            /*
             *   (1) Two Matrix
             */
            if(sign === Matrix.OPE_ADD || sign === Matrix.OPE_SUB) {
                tmp = {
                    xmin: Math.min(
                        (a._value[0] || []).length,
                        (b._value[0] || []).length
                    ),
                    ymin: Math.min(a._value.length, b._value.length)
                };
                for (i = 0; i < tmp.ymin; i++) {
                    result[i] = [];
                    for (j = 0; j < tmp.xmin; j++) {
                        result[i][j] =
                            sign == 'add'
                                ? a._value[i][j].add(b._value[i][j])
                                : a._value[i][j].sub(b._value[i][j]);
                    }
                }
            }
            if(sign === Matrix.OPE_MUL) {
                tmp = {
                    xmin: Math.min(
                        b._value.length,
                        (a._value[0] || []).length
                    ),
                    ymin: Math.min(
                        a._value.length,
                        (b._value[0] || []).length
                    ),
                    ymax: Math.max(
                        a._value.length,
                        (b._value[0] || []).length
                    )
                };
                for (i = 0; i < tmp.ymax; i++) {
                    result[i] = [];
                    for (j = 0; j < tmp.ymin; j++) {
                        result[i][j] = new Num(0);
                        for (k = 0; k < tmp.xmin; k++) {
                            result[i][j] = result[i][j].add(
                                a._value[i][k].mul(b._value[k][j])
                            );
                        }
                    }
                }
            }
            if(sign === Matrix.OPE_DIV) {
                console.error('Matrix.__ope::div not implemented');
            }
        } else {
            /*
             *   (2) One Matrix and One Number
             */
            tmp = {
                xmin: (a._value[0] || []).length,
                ymin: a._value.length
            };
            for (i = 0; i < tmp.ymin; i++) {
                result[i] = [];
                for (j = 0; j < tmp.xmin; j++) {
                    if(sign === Matrix.OPE_ADD)
                        result[i][j] = a._value[i][j].add(b);
                    if(sign === Matrix.OPE_SUB)
                        result[i][j] = a._value[i][j].sub(b);
                    if(sign === Matrix.OPE_MUL)
                        result[i][j] = a._value[i][j].mul(b);
                    if(sign === Matrix.OPE_DIV)
                        result[i][j] = a._value[i][j].div(b);
                    if(!result[i][j]) result[i][j] = new Num(0);
                }
            }
        }
        /* Default */
        if(!result.length) result = this._value;
        return new Matrix(result);
    }

    add(obj) {
        return this.__ope(obj, Matrix.OPE_ADD);
    }
    sub(obj) {
        return this.__ope(obj, Matrix.OPE_SUB);
    }
    mul(obj) {
        return this.__ope(obj, Matrix.OPE_MUL);
    }
    div(obj) {
        return this.__ope(obj, Matrix.OPE_DIV);
    }

    /* Transpose matrix */
    // transpose() {
    //     const [xLen, yLen] = this.getDim();
    //     return new Matrix(
    //         new Array(yLen)
    //             .fill(0)
    //             .map((_, j) =>
    //                 new Array(xLen)
    //                     .fill(0)
    //                     .map((_, i) => (this._value[j] || [])[i] || 0)
    //             )
    //     );
    // }

    /* Dimensions */
    reshape(w, h=0, identity=true) {
        if(!h) h = w;
        return new Matrix(
            new Array(h)
                .fill(0)
                .map((_, j) =>
                    new Array(w)
                        .fill(0)
                        .map((_, i) => {
                            const val = (this._value[j] || [])
                            return identity && i === j && typeof val[i] === 'undefined'
                                ? 1 : (val[i] || 0);
                        })
                )
        );
    }

    /* Conversions */

    convert(type) {
        if (type === Matrix.CONVERT_VECTOR) return this.toVector();
        return this;
    }

    toVector() {
        const Num = require('./Num');
        const Vector = require('./Vector');
        /* Conversion mode */
        return new Vector(
            this._value.reduce((accum, cols) => {
                const imax = cols.length;
                for(let i=0; i < imax; i++) {
                    if(!accum[i]) accum[i] = new Num(0);
                    accum[i] = accum[i].add(cols[i]);
                }
                return accum;
            }, [])
        );
    }

    /*
     *  GETTERS
     */

    getValue() {
        return this._value;
    }

    getDim() {
        return [ this.getDimX(), this.getDimY() ];
    }
    getDimX() {
        return this._value.reduce((accum, x) => Math.max(accum, x.length), 0);
    }
    getDimY() {
        return this._value.length;
    }

    /*
     *  SETTERS
     */

    setValue(value) {
        const { Num, Complex, Quaternion } = require('./index');
        /* Force 2 dimensions */
        if (!Array.isArray(value)) {
            value = [];
        } else if (value.length && !Array.isArray(value[0])) {
            value = [value];
        }
        /* Normalization */
        this._value = value
            .filter(row => Array.isArray(row))
            .map(row =>
                row
                    .filter(
                        col =>
                            typeof col === 'number' ||
                            col instanceof Num ||
                            col instanceof Complex ||
                            col instanceof Quaternion
                    )
                    .map(col => (typeof col === 'number' ? new Num(col) : col))
            );
        return this;
    }
}
Matrix.OPE_ADD = 'add';
Matrix.OPE_SUB = 'sub';
Matrix.OPE_MUL = 'mul';
Matrix.OPE_DIV = 'div';
Matrix.CONVERT_VECTOR = 'Vector';

module.exports = Matrix;
