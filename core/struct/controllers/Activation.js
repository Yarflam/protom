/*
 *  Activation - class @goals
 *  An activation function (Sigmoid, Tanh, Relu, Gauss ...).
 */

class Activation {
    constructor(core, model, a, name = '') {
        /* Properties */
        this._model = model;
        this._a = a;
        /* Current value */
        this._value = null;
        /* Access */
        this._name = core.__addRegister(name);
        this._core = core;
    }

    eval(inputs, ...others) {
        const output = this._core.eval([this, ...others], inputs);
        return output.length == 1 ? output[0] : output;
    }

    apply() {
        this._value = this.__ope(this._a.getValue(), this._model);
        return this;
    }

    /*
     *   Operations
     */

    __ope(obj, action) {
        let result;
        const Num = require('../datatypes/Num');
        const one = new Num(1);
        const two = new Num(2);
        /* Apply the functions */
        switch (action) {
            case 'gauss':
                result = new Num(Math.E).pow(
                    obj
                        .pow2()
                        .inv()
                        .div(two)
                );
                break;
            case 'heaviside':
                result = obj._value >= 0 ? 1 : 0;
                break;
            case 'leaky_relu':
                result = Math.max(obj._value, obj._value * 0.01);
                break;
            case 'relu':
                result = Math.max(0, obj._value);
                break;
            case 'sigmoid':
            case 'logistic':
                result = one.div(one.add(new Num(Math.E).pow(obj.inv())));
                break;
            case 'swish':
                result = obj.div(one.add(new Num(Math.E).pow(obj.inv())));
                break;
            case 'tanh':
                result = two
                    .div(one.add(new Num(Math.E).pow(obj.mul(two.inv()))))
                    .add(one.inv());
                break;
            case 'identity':
            default:
                result = obj;
                break;
        }
        return new Num(result);
    }

    /* GETTERS */

    getValue() {
        return this._value;
    }

    getName() {
        return this._name;
    }
}

module.exports = Activation;
