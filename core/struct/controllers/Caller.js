const { Const } = require('../nodes');

/*
 *  Caller
 *  Call a specific method of node
 */

class Caller {
    constructor(core, method, a, params, name = '') {
        /* Properties */
        this._a = a;
        this._method = method;
        this._params = params;
        /* Current value */
        this._value = null;
        /* Access */
        this._name = core.__addRegister(name);
        this._core = core;
        /* Normalization */
        if (!core.isNode(this._a)) {
            this._a = new Const(core, this._a);
        }
    }

    eval(inputs, ...others) {
        const output = this._core.eval([this, ...others], inputs);
        return output.length == 1 ? output[0] : output;
    }

    apply() {
        this._value = this._a.getValue()[this._method](this._params);
        return this;
    }

    /* GETTERS */

    getValue() {
        return this._value;
    }

    getNative() {
        if (this._value === null) return null;
        return this._value.getValue();
    }

    getName() {
        return this._name;
    }
}

module.exports = Caller;
