const { Const } = require('../nodes');

/*
 *  Operation - class @goals
 *  A calculate node in the graph.
 */

class Operation {
    constructor(core, ope, a, b, name = '') {
        /* Properties */
        this._ope = ope;
        this._a = a;
        this._b = b;
        /* Current value */
        this._value = null;
        /* Access */
        this._name = core.__addRegister(name);
        this._core = core;
        /* Normalization */
        if (!core.isNode(this._a)) {
            this._a = new Const(core, this._a);
        }
        if (!core.isNode(this._b)) {
            this._b = new Const(core, this._b);
        }
    }

    eval(inputs, ...others) {
        const output = this._core.eval([this, ...others], inputs);
        return output.length == 1 ? output[0] : output;
    }

    apply() {
        this._value = this._a.getValue().__ope(this._b.getValue(), this._ope);
        return this;
    }

    /* GETTERS */

    getValue() {
        return this._value;
    }

    getNative() {
        if (this._value === null) return null;
        return this._value.getValue();
    }

    getName() {
        return this._name;
    }
}

module.exports = Operation;
