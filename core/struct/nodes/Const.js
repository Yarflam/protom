/*
 *  Const - class @roles
 *  A constant value.
 */

const constMath = {
    pi: Math.PI,
    e: Math.E,
    i: [0, 1]
};

class Const {
    constructor(core, value, name = '', type = 'auto', ...options) {
        const datatypes = require('../datatypes');
        const { Num, Complex, Quaternion, Matrix } = datatypes;
        /* Detect the math constants */
        if (Object.keys(constMath).indexOf(String(value)) >= 0) {
            value = constMath[String(value)];
        }
        /* Dynamic type */
        if (type == 'auto') {
            /* Numeric */
            if (value instanceof Num || typeof value == 'number') {
                type = 'Num';
            }
            /* Complex */
            if (
                value instanceof Complex ||
                (Array.isArray(value) &&
                    value.length <= 2 &&
                    value.filter(x => x instanceof Num || typeof x == 'number')
                        .length == 2)
            ) {
                type = 'Complex';
            }
            /* Quaternion */
            if (
                value instanceof Quaternion ||
                (Array.isArray(value) &&
                    value.length <= 4 &&
                    value.filter(x => x instanceof Num || typeof x == 'number')
                        .length == 4)
            ) {
                type = 'Quaternion';
            }
            /* Vector */
            /* Matrix */
            if (
                value instanceof Matrix ||
                (Array.isArray(value) && (value.length > 2 || type == 'auto'))
            ) {
                type = 'Matrix';
            }
            /* Tensor */
            /* Default */
            if (type == 'auto') {
                type = 'Num';
            }
        }
        /* Initialize the value */
        this._type = type;
        if (datatypes[this._type] || false) {
            this._value = new datatypes[this._type](value, ...options);
        }
        /* Access */
        this._name = core.__addRegister(name);
        this._core = core;
    }

    toString() {
        return this._value.toString();
    }

    /*
     *  GETTERS
     */

    getValue() {
        return this._value;
    }

    getNative() {
        return this._value.getValue();
    }

    getName() {
        return this._name;
    }
}

module.exports = Const;
