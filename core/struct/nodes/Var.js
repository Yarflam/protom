const Const = require('./Const');

/*
 *  Var - class @roles
 *  A dynamic value, it can evolve by optimizer algorithms.
 */

class Var extends Const {
    constructor(core, value, name = '', type = 'auto', ...options) {
        super(core, value, name, type, ...options);
    }

    /*
     *  SETTERS
     */

    setValue(value, ...options) {
        const datatypes = require('../datatypes');
        if (datatypes[this._type] || false) {
            this._value = new datatypes[this._type](value, ...options);
        }
        return this;
    }
}

module.exports = Var;
