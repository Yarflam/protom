const Var = require('./Var');

/*
 *  Placeholder - class @roles
 *  A free space to have fill by inputs.
 */

class Placeholder extends Var {
    constructor(core, name = '', type = 'auto', ...options) {
        super(core, null, name, type, ...options);
    }
}

module.exports = Placeholder;
