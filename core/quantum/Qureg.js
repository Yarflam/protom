const Qubit = require('./Qubit');
const { Complex, Vector, Matrix, Tensor } = require('../struct/datatypes');

/*
 *  Quantum Register - class @quantum
 *  A Quantum space with multiple qubits.
 *  Note:
 *      - used little endian bit ordering
 */
class Qureg {
    constructor() {
        this._register = new Vector();
        this._states = null;
        this._size = 0;
    }

    toString() {
        return this.getStates().toString();
    }

    debug(target = Qureg.DEBUG_STATES) {
        if (target === Qureg.DEBUG_STATES) {
            return `states = ${this.toString()}`;
        }
        if (target === Qureg.DEBUG_STATES_FULL) {
            return this.getStates()
                .getValue()
                .map(
                    (state, i) =>
                        `|${i
                            .toString(2)
                            .padStart(this._size, '0')}> = ${state.toString()}`
                )
                .join('\n');
        }
        if (target === Qureg.DEBUG_QUBITS) {
            return this._register
                .getValue()
                .map((qubit, i) => `Q${i} = ${qubit.toString()}`)
                .join('\n');
        }
    }

    addQubit(...args) {
        let qubit = args[0] instanceof Qubit
            ? args[0] : new Qubit(...args);
        this._register.push(qubit);
        ++this._size;
        /* Update entanglement */
        if(this._states) {
            this._states = this.__entanglement([qubit], this._states);
        }
        return qubit;
    }

    /* Quantum operations */

    __persitStates() {
        this._states = this.__entanglement();
        return this;
    }

    __entanglement(qubits = this._register.getValue(), states = new Tensor([1])) {
        const imax = qubits.length;
        for(let i=0; i < imax; i++) {
            states = new Tensor(qubits[i].getValue()).product(states);
        }
        return states;
    }

    __gate(qubits, gate) {
        let result;
        /* Get the qubits */
        qubits = qubits.map(target => {
            if (target instanceof Qubit) return target;
            return this.getQubit(target);
        });
        if (qubits.indexOf(null) >= 0) return this;
        /* Applying on qubit */
        if(qubits.length === 1 && qubits[0].isNotZero()) {
            result = new Vector(qubits[0].getValue())
                .toMatrix()
                .mul(gate)
                .getValue();
            qubits[0].setValue(
                result[0][0].add(result[1][0]),
                result[0][1].add(result[1][1])
            );
        }
        /* Applying on states */
        if(this._states || qubits.length > 1) {
            if(!this._states) this.__persitStates();
            /* Adapt the gate */
            let gateSize = Math.log2(gate.getDimY());
            if(this._size > gateSize) {
                if(qubits.length === 1) {
                    gate = new Tensor([ [ 1, 0 ], [ 0, 1 ] ]).product(gate).toMatrix();
                    if(this._size % 2) {
                        gate = gate.reshape(Math.pow(2, this._size)); // fast fix - not sure
                    }
                } else {
                    gate = gate.reshape(Math.pow(2, this._size));
                }
                // console.log(gate.toString());
                gateSize = Math.log2(gate.getDimY());
            }
            if(this._size !== gateSize) {
                console.log('The door can\'t be used.');
                return;
            }
            /* Permut */
            // gate = this.__orderGate(gate, qubits);
            /* Calculate */
            result = new Tensor(
                this._states.toVector().toMatrix().mul(gate).toVector().normalize()
            );
            this._states = result;
            qubits.forEach(qubit => qubit.setValue(0, 0));
        }
        return this;
    }

    __orderGate(gate, qubits, origin=this.getQubits()) {
        // const indexes = qubits.map(qubit => origin.indexOf(qubit));
        // DO SOMETHING HERE
        return gate;
    }

    __switchEndian(gate) {
        if(Array.isArray(gate)) gate = new Matrix(gate);
        if(!(gate instanceof Matrix)) return gate;
        /* Measure */
        const [ dimX, dimY ] = gate.getDim();
        const dimSize = Math.log2(dimX);
        const fromEncoding = new Array(dimX).fill(0)
            .map((_, i) => i.toString(2).padStart(dimSize, '0'));
        const toEncoding = fromEncoding.map(x => x.split('').reverse().join(''));
        const convert = fromEncoding.map(x => toEncoding.indexOf(x));
        /* Transform */
        const mtx = gate.getValue();
        return new Matrix(
            new Array(dimY).fill(0)
                .map((_, j) => {
                    return new Array(dimX).fill(0)
                        .map((_, i) => {
                            return mtx[Number(convert[j])][Number(convert[i])];
                        })
                })
        );
    }

    /* Quantum Gates */

    pauliX(qa) {
        return this.__gate(
            [qa],
            new Matrix([
                [0, 1],
                [1, 0]
            ])
        );
    }
    sigmaX(qa) { return this.pauliX(qa); }
    X(qa) { return this.pauliX(qa); }

    pauliY(qa) {
        return this.__gate(
            [qa],
            new Matrix([
                [0, new Complex(0, -1)],
                [new Complex(0, 1), 0]
            ])
        );
    }
    sigmaY(qa) { return this.pauliY(qa); }
    Y(qa) { return this.pauliY(qa); }

    pauliZ(qa) {
        return this.__gate(
            [qa],
            new Matrix([
                [1, 0],
                [0, -1]
            ])
        );
    }
    sigmaZ(qa) { return this.pauliZ(qa); }
    Z(qa) { return this.pauliZ(qa); }

    hadamard(qa) {
        const hv = 1 / Math.sqrt(2);
        return this.__gate(
            [qa],
            new Matrix([
                [hv, hv],
                [hv, -hv]
            ])
        );
    }
    H(qa) { return this.hadamard(qa); }

    sqrtNot(qa) {
        const snU = new Complex(1, 1).div(2);
        const snV = new Complex(1, -1).div(2);
        return this.__gate(
            [qa],
            new Matrix([
                [snU, snV],
                [snV, snU]
            ])
        );
    }

    phase(qa, angle=Math.PI/2) {
        return this.__gate(
            [qa],
            new Matrix([
                [1, 0],
                [0, new Complex(0, angle).exp()]
            ])
        );
    }
    phaseShift(qa, angle) { return this.phase(qa, angle); }
    P(qa, angle) { return this.phase(qa, angle); }

    sgate(qa) { return this.phase(qa, Math.PI/2); }
    S(qa) { return this.sgate(qa); }

    tgate(qa) { return this.phase(qa, Math.PI/4); }
    T(qa) { return this.tgate(qa); }

    igate(qa) {
        return this.__gate(
            [qa],
            new Matrix([
                [1, 0],
                [0, 1]
            ])
        );
    }
    I(qa) { return this.igate(qa); }

    CNot(qcontrol, qa) {
        return this.__gate(
            [qcontrol, qa],
            this.__switchEndian([
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 0, 0, 0, 1 ],
                [ 0, 0, 1, 0 ]
            ])
        );
    }
    CN(qcontrol, qa) { return this.CNot(qcontrol, qa); }

    ctrlZ(qcontrol, qa) {
        return this.__gate(
            [qcontrol, qa],
            this.__switchEndian([
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 0, 0, 0, -1 ]
            ])
        );
    }
    CZ(qcontrol, qa) { return this.ctrlZ(qcontrol, qa); }

    swap(qa, qb) {
        return this.__gate(
            [qa, qb],
            this.__switchEndian([
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 0, 1, 0, 0 ],
                [ 0, 0, 0, 1 ]
            ])
        );
    }

    sqrtSwap(qa, qb) {
        const snU = new Complex(1, 1).div(2);
        const snV = new Complex(1, -1).div(2);
        return this.__gate(
            [qa, qb],
            this.__switchEndian([
                [ 1, 0, 0, 0 ],
                [ 0, snU, snV, 0 ],
                [ 0, snV, snU, 0 ],
                [ 0, 0, 0, 1 ]
            ])
        );
    }

    toffoli(qcontrol, qa, qb) {
        return this.__gate(
            [qcontrol, qa, qb],
            this.__switchEndian([
                [ 1, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 1, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 1, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 1, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 1, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 1, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 1 ],
                [ 0, 0, 0, 0, 0, 0, 1, 0 ]
            ])
        );
    }

    fredkin(qcontrol, qa, qb) {
        return this.__gate(
            [qcontrol, qa, qb],
            this.__switchEndian([
                [ 1, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 1, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 1, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 1, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 1, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 1, 0 ],
                [ 0, 0, 0, 0, 0, 1, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 1 ]
            ])
        );
    }

    /* GETTERS */

    getQubits() {
        return this._register.getValue();
    }

    getQubit(index) {
        return this.getQubits()[index] || null;
    }

    getStates() {
        return this._states || this.__entanglement();
    }

    // getQubitStates(qubit) {
    //     const states = this.getStates();
    //     if(!(qubit instanceof Qubit)) return states;
    //     /* Find the qubit */
    //     const index = this._register.getValue().indexOf(qubit);
    //     if(index < 0) return states;
    //     /* Return the element */
    //     return;
    // }

    getLength() {
        return this._size;
    }
}
Qureg.DEBUG_STATES = 'states';
Qureg.DEBUG_STATES_FULL = 'states_full';
Qureg.DEBUG_QUBITS = 'qubits';

module.exports = Qureg;
