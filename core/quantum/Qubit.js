const { Complex } = require('../struct/datatypes');

/*
 *  Qubit - class @quantum
 */
class Qubit {
    constructor(up = 0, down = 0) {
        this.setValue(up, down);
    }

    toString() {
        if(!this.isNotZero()) return 'α|0> + β|1>';
        return `${this._up.toString()}|0> + ${this._down.toString()}|1>`;
    }

    isNotZero() {
        return this._up.isNotZero() || this._down.isNotZero();
    }

    /* GETTERS */

    getValue() {
        return [this._up, this._down];
    }

    getUp() {
        return this._up;
    }

    getDown() {
        return this._down;
    }

    /* SETTERS */

    setValue(up = 0, down = 0) {
        this._up = up instanceof Complex ? up : new Complex(up);
        this._down = down instanceof Complex ? down : new Complex(down);
        return this;
    }
}

module.exports = Qubit;
