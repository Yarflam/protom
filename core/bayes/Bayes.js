/*
 *   Bayes Theorem
 *       dev version
 */

class Bayes {
    constructor() {
        this._epsilon = 0.000001;
        this._theory = [];
        this._prior = [];
    }

    add(theory) {
        if (typeof theory == 'number') {
            this._theory.push(theory);
            return true;
        }
        return false;
    }

    next() {
        if (this._theory.length == this._prior.length) {
            /* Execute the Bayes Theorem */
            const result = this._theory.map(
                (t, i) => t * this._prior[i] || this._epsilon
            );
            const sum = result.reduce((accum, x) => accum + x, 0);
            this._prior = result.map(x => x / (sum || 1));
            this._theory = [];
        } else if (this._theory.length) {
            /* Move the theories */
            this._prior = this._theory.map(x => x);
            this._theory = [];
        }
        return this._prior;
    }

    setPrior(index, x) {
        if (index < this._prior.length) {
            this._prior[index] = x;
            return true;
        }
        return false;
    }
}

module.exports = Bayes;
