var w = window,
    d = document,
    ael = 'addEventListener',
    qs = 'querySelector',
    ih = 'innerHTML';

w[ael]('load', function() {
    var paper = d[qs]('#paper');

    /* Paper */
    d[qs]('h1')[ih] += ' - v' + Protom.Version + '_dev';
    paper[ih] += '<p><b>Authors:</b> ' + Protom.Authors.join(', ') + '</p>';
    paper[ih] +=
        '<p><b>Protom Lab:</b> ' + Object.keys(Protom.lab).join(', ') + '</p>';

    /* Example */
    var app = new Protom(),
        x = app.placeholder('x'),
        a = app.var(4, 'a'),
        b = app.const(5, 'b');

    var y = x.mul(a).add(b),
        calc = y.eval({ x: 3 }, x);

    paper[ih] += '<p><b>Example:</b></p>';
    paper[ih] +=
        '<p>Graph(Placeholder.x * Var.a + Const.b) = ' +
        calc[1].getValue() +
        ' * ' +
        a.getValue() +
        ' + ' +
        b.getValue() +
        ' = ' +
        calc[0].getValue() +
        '</p>';

    /* Quantum Example */
    var Qureg = Protom.lab.quantum.Qureg;
    var qreg = new Qureg();
    qreg.addQubit(1, 0);
    qreg.addQubit(1, 0);
    qreg.addQubit(1, 0);
    
    paper[ih] += '<p><b>Quantum Experiment:</b></p>';
    paper[ih] += '<p>Qubits - <b>initial state</b><br/>' +
        qreg.debug(Qureg.DEBUG_QUBITS).replace(/\n/g,'<br/>') + '</p>';

    paper[ih] += '<p>&gt; Hadamard 0<br/>&gt; Hadamard 1<br/>&gt; Phase 1</p>';
    qreg.hadamard(0).hadamard(1).phase(1);

    paper[ih] += '<p>Qubits<br/>' +
        qreg.debug(Qureg.DEBUG_QUBITS).replace(/\n/g,'<br/>') + '</p>';
    paper[ih] += '<p>States<br/>' +
        qreg.debug(Qureg.DEBUG_STATES_FULL).replace(/\n/g,'<br/>') + '</p>';
});
