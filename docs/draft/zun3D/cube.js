import yengin from 'yengin';
import Zun from './zun';

export default function Cube (args) {
    var self = {}, priv = {};

    var u = 0.5;

    priv.build_points = [
        [-u,-u,-u],[u,-u,-u],[u,u,-u],[-u,u,-u],
        [-u,-u,u],[u,-u,u],[u,u,u],[-u,u,u],
    ];

    priv.build_edges = [
        [0,1],[1,2],[2,3],[3,0],
        [4,5],[5,6],[6,7],[7,4],
        [0,4],[1,5],[2,6],[3,7]
    ];

    priv.build_faces = [
        [0,1,2,3], [4,5,6,7],
        [0,1,5,4], [2,3,7,6],
        [0,3,7,4], [1,2,6,5]
    ];

    /* Constructor */

    priv.init = function (args) {
        var obj; args = args||{};
        if(yengin.isset(args.useFace) && !args.useFace) { priv.build_faces = []; }
        obj = new Zun.Shape(priv.build_points, priv.build_edges, priv.build_faces);
        obj.legacy(self);
        return obj;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Cube'};
    return priv.init(args);
}
