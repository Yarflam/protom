import Zun from './zun';

export default function Torus (args) {
    var self = {}, priv = {},
        tau = Math.PI*2;

    /* Constructor */

    priv.init = function (args) {
        var obj; args = args||{};
        args.in = args.in>>0||3;
        args.out = args.out>>0||1;
        args.inFit = args.inFit>>0||10;
        args.outFit = args.outFit>>0||10;
        /* Size: 1 */
        args.gsize = 0.5/(args.in+args.out);
        args.in *= args.gsize;
        args.out *= args.gsize;
        /* Construct */
        var i, j, pt, points=[], edges=[], faces=[];
        for(i=0; i < args.inFit; i++) {
            /* Horizontal Circle */
            pt = [
                Math.cos( tau / args.inFit * i ) * args.in,
                Math.sin( tau / args.inFit * i ) * args.in,
                0
            ]
            /* Vertical Circles */
            for(j=0; j < args.outFit; j++) {
                points.push([
                    pt[0] - Math.cos( tau / args.outFit * j ) * args.out
                        * Math.sin( tau / args.inFit * ((i + args.inFit*0.25) % args.inFit) ),
                    pt[1] + Math.cos( tau / args.outFit * j ) * args.out
                        * Math.cos( tau / args.inFit * ((i + args.inFit*0.25) % args.inFit) ),
                    pt[2] + Math.sin( tau / args.outFit * j ) * args.out
                ]);
                edges.push([ j + i * args.outFit, (j < args.outFit-1 ? j+1 : 0) + i * args.outFit ]);
                /* Link between two vertical circles */
                edges.push([ j + i * args.outFit, j + (i < args.inFit-1 ? i+1 : 0) * args.outFit ]);
                /* Face */
                faces.push([
                    j + i * args.outFit,
                    (j < args.outFit-1 ? j+1 : 0) + i * args.outFit,
                    (j < args.outFit-1 ? j+1 : 0) + (i < args.inFit-1 ? i+1 : 0) * args.outFit,
                    j + (i < args.inFit-1 ? i+1 : 0) * args.outFit
                ]);
            }
        }
        /* Build */
        obj = new Zun.Shape(points, edges, faces);
        obj.legacy(self).setPivot([0, 0, 0]);
        return obj;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Torus'};
    return priv.init(args);
}
