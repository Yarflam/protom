import Zun from './zun';

export default function Grid (w, h) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (w, h) {
        var wh, points = [], edges = [];
        w = (w||10)+1, h = (h||10)+1,
        wh = w*h;
        /* Generate the grid */
        for(var x, y, i=0; i < wh; i++) {
            x = (i % w);
            y = Math.floor(i / w);
            /* Points */
            points.push([ (1/(w-1)) * x-((w-1)/2), (1/(h-1)) * y-((h-1)/2), 0 ]);
            /* Edges */
            if(i) {
                /* Horizontal */
                if(x) { edges.push([ i-1, i ]); }
                /* Vertical */
                if(y) { edges.push([ i-w, i ]); }
            }
        }
        /* Create the shape */
        var obj = new Zun.Shape(points, edges, []);
        obj.legacy(self);
        return obj;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Grid'};
    return priv.init(w, h);
}
