import yengin from 'yengin';

export default function Animate () {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function () {
        /* Properties */
        priv._listEvents = [];
        priv._listLabels = [];
        priv._fps = 60;
    };

    /* Public Methods */

    self.add = function (callback, label) {
        if(yengin.istype(callback, 'Function')) {
            /* Add a label */
            priv._listLabels.push((yengin.isset(label) ? label : '_'));
            /* Add an event */
            priv._listEvents.push({
                event: null,
                action: callback
            });
        }
        /* Return the object */
        return self;
    };

    self.remove = function (label) {
        var events = self.get(label);
        yengin.each(events, function (obj) {
            /* If is executed */
            if(obj.event !== null) {
                yengin.stopWait(obj.event);
                obj['event'] = null;
            }
            /* Remove it */
            var index = priv._listEvents.indexOf(obj);
            priv._listEvents.splice(index, 1);
            priv._listLabels.splice(index, 1);
        });
    };

    self.start = function (label) {
        var events = self.get(label);
        yengin.each(events, function (obj) {
            /* Is not executed */
            if(obj.event === null) {
                var sframe, frame = 0,
                    ratio = priv._fps/1000;
                var evt = function () {
                    sframe = Math.floor(ratio * (yengin.getTime() % 1000));
                    if(sframe != frame) {
                        frame = sframe;
                        obj.action();
                    }
                };
                obj['event'] = yengin.wait(evt, 0, true);
            }
        });
    };

    self.stop = function (label) {
        var events = self.get(label);
        yengin.each(events, function (obj) {
            /* Is executed */
            if(obj.event !== null) {
                yengin.stopWait(obj.event);
                obj['event'] = null;
            }
        });
    };

    self.state = function (label) {
        var n=0, events = self.get(label);
        yengin.each(events, function (obj) {
            if(obj.event !== null) { n++; }
        });
        /* Return a ratio */
        return (1/events.length) * n;
    };

    self.get = function (label) {
        if(yengin.isset(label)) {
            if(yengin.istype(label, 'String')) {
                /* Type: String */
                var index = priv._listLabels.indexOf(label);
                if(index >= 0) {
                    /* Is found */
                    return [priv._listEvents[index]];
                } else { return []; }
            } else if(yengin.istype(label, 'Number')) {
                /* Type: Number */
                if(yengin.isset(priv._listEvents[label])) {
                    /* Is found */
                    return [priv._listEvents[label]];
                } else { return []; }
            }
        }
        /* All events */
        return priv._listEvents;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Animate'};
    return (priv.init(), self);
}
