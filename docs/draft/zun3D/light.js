export default function Light () {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function () {};

    /* Signature */
    self.constructor = {'name': 'Zun.Light'};
    return (priv.init(), self);
}
