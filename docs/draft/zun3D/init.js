import Zun from './zun';
import Point from './point'; // XYZ position
import Vector from './vector'; // C + R/T + S/G
import Transform from './transform'; // R/S/T
import Particle from './particle'; // ... working ...
import Edge from './edge'; // R/S/T + S/G
import Face from './face'; // Material + Each + R/S/T + S/G
import Shape from './shape'; // Builder + Each + R/S/T + S/G
import Plot from './plot'; // Generator PEF 3D
import Cube from './cube'; // PEF 3D
import Grid from './grid'; // Generator PEF 3D
import Torus from './torus'; // Generator PEF 3D
import Cylinder from './cylinder'; // Generator PEF 3D
import MengerSponge from './mengersponge'; // ... working ...
import LSystem from './lsystem'; // ... working ...
import Surface from './surface'; // PEF 3D
import Material from './material'; // Colors + Opacity + S/G
// import Light from './light'; // ... working ...
import Gpu from './gpu'; // ... [ Experiment ] ...
import Camera from './camera'; // Render 2D/3D + Zoom + R/S/T + Cache + S/G
import Scene from './scene'; // Manage : Camera, Shape, FPS
import Animate from './animate'; // Manage : Animation Start/Stop, FPS
import Logs from './logs'; // Manage : Logs
// import Project from './project'; // ... working ...

Zun.Point = Point;
Zun.Vector = Vector;
Zun.Transform = Transform;
Zun.Particle = Particle;
Zun.Edge = Edge;
Zun.Face = Face;
Zun.Shape = Shape;
Zun.Plot = Plot; // !!
Zun.Cube = Cube;
Zun.Grid = Grid;
Zun.Torus = Torus;
Zun.Cylinder = Cylinder;
Zun.MengerSponge = MengerSponge;
Zun.LSystem = LSystem;
Zun.Surface = Surface;
Zun.Material = Material;
// Zun.Light = Light;
Zun.Gpu = Gpu;
Zun.Camera = Camera;
Zun.Scene = Scene;
Zun.Animate = Animate;
Zun.Logs =  Logs;
// Zun.Project = Project;

export default Zun;

/*
*	Notations
*	C : Create
*	R/S/T : Rotate / Scale / Translate
*	S/G : Setters / Getters
*	PEF : Points, Edges, Faces
*/
