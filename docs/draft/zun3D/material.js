import yengin from 'yengin';

export default function Material (args) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (args) {
        args = args||{};

        /* Properties */
        priv._colors = [255, 140, 0];
        priv._opacity = 0.6;

        /* RGB */
        if(args.rgb) { self.setColors(args.rgb); }

        /* RGBA */
        if(args.rgba && Array.isArray(args.rgba) && args.rgba.length == 4) {
            self.setColors(args.rgba.slice(0,3));
            self.setOpacity(args.rgba[3]);
        }
    };

    /* Public Methods */

    self.toRgba = function () {
        return 'rgba('+[
            priv._colors[0],
            priv._colors[1],
            priv._colors[2],
            priv._opacity,
        ].join(',')+')';
    };

    /* Getters */

    self.getColors = function () {
        return priv._colors;
    };

    self.getOpacity = function () {
        return priv._opacity;
    };

    /* Setters */

    self.setColors = function (colors) {
        if(yengin.isset(colors) && yengin.istype(colors, 'Array')) {
            if(colors.length == 3 || colors.length == 4) {
                priv._colors = [
                    Number(colors[0]),
                    Number(colors[1]),
                    Number(colors[2]),
                ];
                self.setOpacity(colors[3]);
            }
        } return self;
    };

    self.setOpacity = function (opacity) {
        if(yengin.isset(opacity) && yengin.istype(opacity, 'Number')) {
            opacity = Number(opacity);
            if(opacity >= 0 && opacity <= 1)
                priv._opacity = opacity;
        } return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Material'};
    return (priv.init(args), self);
}
