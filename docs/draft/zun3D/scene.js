import yengin from 'yengin';

export default function Scene () {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function () {
        priv._cameras = [];
        priv._shapes = [];
        priv._particles = [];
        priv._fps = { start: 0, img: 0, total: 0 };
    };

    /* Public Methods */

    self.render = function () {
        priv.evalFps();
        for(var i=0; i < priv._cameras.length; i++) {
            priv._cameras[i].render(self);
        } return self;
    };

    self.eachShapes = function (callback) {
        return (yengin.each(priv._shapes, callback), self);
    };

    self.eachParticles = function (callback) {
        return (yengin.each(priv._particles, callback), self);
    };

    self.getFps = function () {
        return priv._fps.total;
    };

    /* Private Methods */

    priv.evalFps = function () {
        var d = (yengin.getTime()-priv._fps.start);
        if(!priv._fps.start || d >= 2000) { priv._fps.start = yengin.getTime(); }
        if(d >= 1000) { priv._fps = { start: yengin.getTime(), img: 0, total: priv._fps.img }; }
        priv._fps.img++;
    };

    /* Setters */

    self.add = function (obj) {
        switch(obj.constructor.parent||obj.constructor.name) {
            case 'Zun.Camera':
                self.addCamera(obj); break;
            case 'Zun.Shape':
                self.addShape(obj); break;
            case 'Zun.Particle':
                self.addParticle(obj); break;
            default: break;
        } return self;
    };

    self.addCamera = function (camera) {
        if(camera.constructor.name == 'Zun.Camera') {
            priv._cameras.push(camera);
            camera.setScene(self);
        } return self;
    };

    self.addShape = function (shape) {
        if(shape.constructor.parent == 'Zun.Shape') {
            priv._shapes.push(shape);
        } return self;
    };

    self.addParticle = function (particle) {
        if(particle.constructor.name == 'Zun.Particle') {
            priv._particles.push(particle);
        } return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Scene'};
    return (priv.init(), self);
}
