import yengin from 'yengin';
import Zun from './zun';

export default function Shape (points, edges, faces) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (points, edges, faces) {
        points = points||[],
        edges = edges||[],
        faces = faces||[];

        /* Properties */
        priv._scale = 1;
        priv._vector = new Zun.Vector();

        priv._pivot = null; // test

        /* Matrix objects */
        priv._points = [];
        priv._edges = [];
        priv._faces = [];

        /* Build methods */
        if(points.length)
            self.build(points, edges, faces);
    };

    /* Public Methods */

    self.build = function (points, edges, faces) {
        var i, j, index;
        /* Create the points (0: x, 1: y, 2: z) */
        for(i=0; i < points.length; i++) {
            points[i] = new Zun.Point(points[i][0], points[i][1], points[i][2]);
        }
        /* Create the edges */
        for(i=0; i < edges.length; i++) {
            if(edges[i].length == 2) {
                var edge = [];
                /* Add the two points */
                if(yengin.isset(points[edges[i][0]]))
                    edge.push(points[edges[i][0]]);
                if(yengin.isset(points[edges[i][1]]))
                    edge.push(points[edges[i][1]]);
                /* Build the edge */
                edges[i] = new Zun.Edge(edge);
            }
        }
        /* Create the faces */
        for(i=0; i < faces.length; i++) {
            if(faces[i].length >= 3) {
                var face = [];
                /* Add the points */
                for(j=0; j < faces[i].length; j++) {
                    index = faces[i][j];
                    if(yengin.isset(points[index])) {
                        face.push(points[index]);
                    }
                }
                /* Build the face */
                faces[i] = new Zun.Face(face);
            }
        }
        /* Add in the shape */
        priv._points = points;
        priv._edges = edges;
        priv._faces = faces;
        /* Set the pivot */
        self.getPivot();
        /* Remove this method */
        delete self.build;
        /* Return the object */
        return self;
    };

    self.legacy = function (obj) {
        var item;
        for(item in obj)
            self[item] = obj[item];
        self.constructor = {'name': obj.constructor.name, 'parent': 'Zun.Shape'};
        /* Remove this method */
        delete self.legacy;
        /* Return the object */
        return self;
    };

    /* Rotate the shape on XYZ */
    self.rotate = function (x, y, z) {
        return self.rotateX(x).rotateY(y).rotateZ(z);
    };

    /* Rotate the shape on the axis X */
    self.rotateX = function (angle) {
        priv._vector.rotateX(angle);
        Zun.Transform.rotateX(priv._points, angle, self.getPivot());
        return self;
    };

    /* Rotate the shape on the axis Y */
    self.rotateY = function (angle) {
        priv._vector.rotateY(angle);
        Zun.Transform.rotateY(priv._points, angle, self.getPivot());
        return self;
    };

    /* Rotate the shape on the axis Z */
    self.rotateZ = function (angle) {
        priv._vector.rotateZ(angle);
        Zun.Transform.rotateZ(priv._points, angle, self.getPivot());
        return self;
    };

    /* Move the shape from the current point (relative location) */
    self.move = function (x, y, z) {
        priv._vector.move(x, y, z);
        Zun.Transform.move(priv._points, x, y, z);
        return self;
    };

    /* Move the shape to a specific location */
    self.moveTo = function (x, y, z) {
        var pivot = self.getPivot();
        priv._vector.moveTo(x, y, z);
        Zun.Transform.moveTo(priv._points.concat(pivot), x, y, z, pivot);
        return self;
    };

    /* Change the scale */
    self.resize = function (ratio) {
        if(yengin.isset(ratio) && yengin.istype(ratio, 'Number')) {
            Zun.Transform.resize(priv._points, ratio, self.getPivot());
            priv._scale *= ratio;
        }
        return self;
    };

    /* Get the shape's central point (average) */
    self.getPivot = function (force) {
        if(priv._pivot === null || (force||false))
            priv._pivot = Zun.Transform.getPivot(priv._points);
        return priv._pivot;
    };

    /* Inspect the object */
    self.inspect = function (callback) {
        callback.bind({
            points: priv._points,
            edges: priv._edges,
            faces: priv._faces,
            eachPoints: self.eachPoints,
            eachEdges: self.eachEdges,
            eachFaces: self.eachFaces
        })();
    };

    /* Each points */
    self.eachPoints = function (callback) {
        return (yengin.each(priv._points, callback), self);
    };

    /* Each edges */
    self.eachEdges = function (callback) {
        return (yengin.each(priv._edges, callback), self);
    };

    /* Each faces */
    self.eachFaces = function (callback) {
        return (yengin.each(priv._faces, callback), self);
    };

    /* Add a gyroscope */
    self.addGyroscope = function (weight) {
        /* Get the max. distance */
        var pivot = priv._pivot.get(), maxDist = 0;
        self.eachPoints(function (point) {
            maxDist = Math.max(maxDist, point.get().sub(pivot).dist());
        });
        maxDist += 0.5;
        /* Set the points */
        var points = [
            new Zun.Point().set(pivot),
            new Zun.Point().set(pivot.add(0, maxDist)),
            new Zun.Point().set(pivot.add(0, 0, maxDist)),
            new Zun.Point().set(pivot.add(0, 0, 0, maxDist)),
        ];
        /* Set the axes */
        var axeX = new Zun.Edge([ points[0], points[1] ]),
            axeY = new Zun.Edge([ points[0], points[2] ]),
            axeZ = new Zun.Edge([ points[0], points[3] ]);
        /* Define the weight and colors */
        axeX.setWeight(weight>>0||3).getMaterial().setColors([200,0,0,1]);
        axeY.setWeight(weight>>0||3).getMaterial().setColors([0,200,0,1]);
        axeZ.setWeight(weight>>0||3).getMaterial().setColors([0,0,200,1]);
        /* Add them */
        priv._points = priv._points.concat(points);
        priv._edges.push(axeX);
        priv._edges.push(axeY);
        priv._edges.push(axeZ);
        return self;
    };

    /* Getters */

    self.getScale = function () {
        return priv._scale;
    };

    self.getVector = function () {
        return priv._vector;
    };

    self.getPoints = function () {
        return priv._points;
    };

    self.getEdges = function () {
        return priv._edges;
    };

    self.getFaces = function () {
        return priv._faces;
    };

    /* Setters */

    self.setVector = function (vector) {
        if(yengin.isset(vector) && vector.constructor.name == 'Zun.Vector') {
            priv._vector = vector;
        } return self;
    };

    self.setPoints = function (points) {
        if(yengin.istype(points, 'Array'))
            priv._points = points;
        return self;
    };

    self.setEdges = function (points, edges) {
        if(yengin.istype(points, 'Array') && yengin.istype(edges, 'Array')) {
            priv._points = points;
            priv._edges = edges;
        }
        return self;
    };

    self.setFaces = function (points, edges, faces) {
        if(yengin.istype(points, 'Array')
        && yengin.istype(edges, 'Array')
        && yengin.istype(faces, 'Array')) {
            priv._points = points;
            priv._edges = edges;
            priv._faces = faces;
        }
        return self;
    };

    self.setMaterial = function (material) {
        if(yengin.isset(material) && material.constructor.name == 'Zun.Material') {
            self.eachFaces(function (face) {
                face.setMaterial(material);
            });
        } return self;
    };

    self.setPivot = function (point) {
        if(yengin.istype(point, 'Array') && point.length == 3) {
            priv._pivot = new Zun.Point(point[0], point[1], point[2]);
        } return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Shape'};
    return (priv.init(points, edges, faces), self);
}
