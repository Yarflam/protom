import yengin from 'yengin';
import Zun from './zun';

export default (function Transform () {
    var self = {};

    /* Rotate the points on the axis X */
    self.rotateX = function (points, angle, pivot) {
        var i; pivot = pivot||self.getPivot(points);
        for(i=0; i < points.length; i++) {
            points[i].set(
                points[i].get().sub(pivot.get()).rotateX(angle-180).add(pivot.get())
            );
        }
    };

    /* Rotate the points on the axis Y */
    self.rotateY = function (points, angle, pivot) {
        var i; pivot = pivot||self.getPivot(points);
        for(i=0; i < points.length; i++) {
            points[i].set(
                points[i].get().sub(pivot.get()).rotateY(angle-180).add(pivot.get())
            );
        }
    };

    /* Rotate the points on the axis Z */
    self.rotateZ = function (points, angle, pivot) {
        var i; pivot = pivot||self.getPivot(points);
        for(i=0; i < points.length; i++) {
            points[i].set(
                points[i].get().sub(pivot.get()).rotateZ(angle-180).add(pivot.get())
            );
        }
    };

    /* Move the points from the current point (relative location) */
    self.move = function (points, x, y, z) {
        for(var i=0; i < points.length; i++)
            points[i].set(points[i].get().add(0,x,y,z));
    };

    /* Move the points to a specific location */
    self.moveTo = function (points, x, y, z, pivot) {
        var i, dist; pivot = pivot||self.getPivot(points);
        dist = yengin.quaternion(0,x,y,z).sub(pivot.get());
        for(i=0; i < points.length; i++)
            points[i].set(points[i].get().add(dist));
    };

    /* Change the scale */
    self.resize = function (points, ratio, pivot) {
        var i; pivot = pivot||self.getPivot(points);
        for(i=0; i < points.length; i++) {
            points[i].set(
                points[i].get().sub(pivot.get()).mul(ratio).add(pivot.get())
            );
        }
    };

    /* Get the center of a points cloud (average) */
    self.getPivot = function (points) {
        var i, cutPivot, pivot = new Zun.Point();
        if(points.length) {
            for(i=0; i < points.length; i++)
                pivot.set(pivot.get().add(points[i].get()));
            cutPivot = pivot.get();
            pivot.set(cutPivot._i/i, cutPivot._j/i, cutPivot._k/i);
        } return pivot;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Transform'};
    return self;
})();
