import yengin from 'yengin';
import Zun from './zun';

export default function Particle (x, y, z) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (x, y, z) {
        /* Default Properties */
        priv._scale = 1;
        priv._material = new Zun.Material({ rgba: [0,0,0,0.3] });

        /* Data */
        priv._point = new Zun.Point(x>>0, y>>0, z>>0);
    };

    /* Public Methods */

    /* Move the particle from the current location */
    self.move = function (x, y, z) {
        priv._point.set(priv._point.get().add(
            yengin.quaternion(0, x, y, z)
        ));
    };

    /* Move the particle to a specific location */
    self.moveTo = function (x, y, z) {
        priv._point = new Zun.Point(x, y, z);
    };

    /* Getters */

    self.getScale = function () {
        return priv._scale;
    };

    self.getMaterial = function () {
        return priv._material;
    };

    self.getPoint = function () {
        return priv._point;
    };

    /* Setters */

    self.setScale = function (scale) {
        return (priv._scale = Number(scale), self);
    };

    self.setMaterial = function (material) {
        if(yengin.isset(material) && material.constructor.name == 'Zun.Material') {
            self.eachFaces(function (face) {
                face.setMaterial(material);
            });
        } return self;
    };

    self.setPoint = function (point) {
        if(yengin.isset(point) && point.constructor.name == 'Zun.Point')
            priv._point = point;
        return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Particle'};
    return (priv.init(x, y, z), self);
}
