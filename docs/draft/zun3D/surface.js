import Zun from './zun';

export default function Surface () {
    var self = {}, priv = {};

    var u = 0.5;

    priv.build_points = [
        [-u,u,0],[u,u,0],
        [u,-u,0],[-u,-u,0]
    ];

    priv.build_edges = [
        [0,1],[1,2],[2,3],[3,0]
    ];

    priv.build_faces = [
        [0,1,2,3]
    ];

    /* Constructor */

    priv.init = function () {
        var obj;
        obj = new Zun.Shape(priv.build_points, priv.build_edges, priv.build_faces);
        obj.legacy(self);
        return obj;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Surface'};
    return priv.init();
}
