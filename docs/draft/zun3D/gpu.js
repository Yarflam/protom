import yengin from 'yengin';
import Zun from './zun';

export default (function Gpu () {
    var self = {}, priv = { _gl: null };

    /* Constructor */

    priv.init = function () {
        if(priv._gl !== null) { return priv._gl||false; }
        /* Create the canvas */
        priv._canvas = yengin.getNewObj('canvas');
        /* Get the context */
        var glProps = { antialias: false };
        priv._gl = priv._canvas.getContext('webgl', glProps)
            || priv._canvas.getContext('experimental-webgl', glProps);
        /* Check the webgl */
        if(!priv._gl || !(priv._gl instanceof WebGLRenderingContext)) {
            return (priv._gl = false, false);
        }
        return true;
    };

    /* Public methods */

    self.run = function (callback) {
        if(priv.init()) {
            var proc = {
                x: 64, y: 64,
                buffers: {},
                vertex: null,
                fragment: null
            };
            /* Methods */
            proc.setBuffer = function (name, data) {
                return (proc.buffers[name] = data, proc); };
            proc.setVertex = function (vertex) {
                return (proc.vertex = vertex, proc); };
            proc.setFragment = function (fragment) {
                return (proc.fragment = fragment, proc); };
            /* Execute the profile */
            callback(proc);
            /* Create and execute the program */
            priv.glProgram(proc);
        }
    };

    self.dev = function () {
        Zun.Gpu.run((proc) => {
            proc.setBuffer('x', [
                (2/64)*30-1, (2/64)*33-1, 0.0,
                (2/64)*31-1, (2/64)*32-1, 0.0,
                (2/64)*32-1, (2/64)*32-1, 0.0,
            ]).setBuffer('y', [
                2.0, 5, 1.0,
                1.0, 1.0, 1.0,
                1.0, 1.0, 1.0,
            ]).setVertex([
                'attribute vec3 x;',
                'attribute vec3 y;',
                'void main(void) {',
                '	gl_Position = vec4(x[0]*y[0], x[1]*y[1], x[2]*y[2], 1.0);',
                '	gl_PointSize = 1.0;',
                '}'
            ].join(' ')).setFragment([
                'void main(void) {',
                '	gl_FragColor = vec4((1.0/255.0)*250.0, (1.0/255.0)*101.0, (1.0/255.0)*2.0, 1.0);',
                '}'
            ].join(' '));
        });
    };

    /* WebGl methods */

    priv.glProgram = function (proc) {
        var pt = 0, gl = priv._gl, program = gl.createProgram();
        /* Vertex Shader */
        priv.glShader(program, gl.VERTEX_SHADER, proc.vertex);
        /* Fragment Shader */
        priv.glShader(program, gl.FRAGMENT_SHADER, proc.fragment);
        /* Use the program */
        gl.linkProgram(program);
        gl.useProgram(program);
        /* Attributes */
        yengin.each(proc.buffers, function (name, data) {
            if(pt && data.length != (pt * 3)) { return; }
            priv.glBuffer(program, name, data);
            pt = data.length/3;
        });
        /* Reset the size */
        priv._canvas.width = proc.x;
        priv._canvas.height = proc.y;
        /* Define the canvas */
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.viewport(0, 0, priv._canvas.width, priv._canvas.height);
        /* Drawing */
        gl.drawArrays(gl.POINTS, 0, pt);
        /* Get the output */
        var pixels = new Uint8Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4);
        gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
        var i, x, y, bf, maxPixel = pixels.length / 4;
        for(i=0; i < maxPixel; i++) {
            bf = [pixels[i*4], pixels[i*4+1], pixels[i*4+2], pixels[i*4+3]];
            if(bf[0] || bf[1] || bf[2]) {
                x = i % proc.x;
                y = Math.floor(i / proc.x);
                console.log('Position: ' + [x+1,y+1]);
                console.log('Colors: ' + bf);
            }
        }
        console.log('Viewport: ' + [ gl.drawingBufferWidth, gl.drawingBufferHeight ]);
    };

    priv.glBuffer = function (program, name, data) {
        if(!yengin.istype(data,'Array') || !data.length) { return; }
        var gl = priv._gl, buffer = gl.createBuffer();
        /* Assign the buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);
        /* Define the attribute */
        var attrib = gl.getAttribLocation(program, name);
        gl.vertexAttribPointer(attrib, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(attrib);
        /* Release */
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    };

    priv.glShader = function (program, type, code) {
        if(!yengin.istype(code,'String') || !code.length) { return; }
        var gl = priv._gl, shader = gl.createShader(type);
        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        /* Attach the shader */
        gl.attachShader(program, shader);
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Gpu'};
    return self;
})();
