import yengin from 'yengin';

export default function Point (x, y, z) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (x, y, z) {
        priv._value = yengin.quaternion(0);
        self.set(x||0, y||0, z||0);
    };

    /* Getters */

    self.get = function () {
        return priv._value;
    };

    self.getX = function () {
        return priv._value._i;
    };

    self.getY = function () {
        return priv._value._j;
    };

    self.getZ = function () {
        return priv._value._k;
    };

    /* Setters */

    self.set = function (x, y, z) {
        if(x.constructor.name == 'Zun.Point') {
            priv._value = x.get();
        } else if(x.constructor.name == 'Quaternion') {
            priv._value = yengin.quaternion(x._w, x._i, x._j, x._k);
        } else {
            priv._value = yengin.quaternion(0, x, y, z);
        } return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Point'};
    return (priv.init(x, y, z), self);
}
