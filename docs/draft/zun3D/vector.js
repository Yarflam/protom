import yengin from 'yengin';
import Zun from './zun';

export default function Vector(location, direction) {
    var self = {},
        priv = {};

    /* Constructor */

    priv.init = function(location, direction) {
        /* Default Properties */
        priv._location = new Zun.Point();
        priv._direction = [0, 0, 0];
        /* Location */
        if (yengin.isset(location)) self.setLocation(location);
        /* Direction */
        if (yengin.isset(direction)) self.setDirection(direction);
    };

    /* Public Methods */

    self.add = function(vector) {
        /* Verify the constructor */
        if (vector.constructor.name != 'Zun.Vector') return self;
        /* Properties */
        var location = vector.getLocation(),
            direction = vector.getDirection();
        /* Rotations */
        self.rotateX(direction[0]);
        self.rotateY(direction[1]);
        self.rotateZ(direction[2]);
        /* Translation */
        priv._location.set(priv._location.get().add(location.get()));
        return self;
    };

    self.rotateX = function(angle) {
        return (
            (priv._direction[0] = yengin.mod(priv._direction[0] + angle, 360)),
            self
        );
    };

    self.rotateY = function(angle) {
        return (
            (priv._direction[1] = yengin.mod(priv._direction[1] + angle, 360)),
            self
        );
    };

    self.rotateZ = function(angle) {
        return (
            (priv._direction[2] = yengin.mod(priv._direction[2] + angle, 360)),
            self
        );
    };

    self.move = function(x, y, z) {
        var pt = new Zun.Point(x, y, z);
        return priv._location.set(priv._location.get().add(pt.get())), self;
    };

    self.moveTo = function(x, y, z) {
        var pt = new Zun.Point(x, y, z);
        return priv._location.set(pt.get()), self;
    };

    /* Getters */

    self.getLocation = function() {
        return priv._location;
    };

    self.getDirection = function() {
        return priv._direction;
    };

    /* Setters */

    self.setLocation = function(location) {
        if (location.constructor.name == 'Zun.Point')
            return priv._location.set(location), self;
        return self;
    };

    self.setDirection = function(direction) {
        if (yengin.istype(direction, 'Array') && direction.length == 3)
            return (priv._direction = direction), self;
        return self;
    };

    /* Signature */
    self.constructor = { name: 'Zun.Vector' };
    return priv.init(location, direction), self;
}
