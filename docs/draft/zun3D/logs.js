import yengin from 'yengin';

export default function Logs (selector) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (selector) {
        priv._data = [];
        priv._target = null;
        priv._limit = 5;
        /* Autoset */
        if(yengin.isset(selector)) {
            self.attach(selector);
        }
    };

    /* Public Methods */

    self.info = function (message) {
        priv._data.push({
            type: 'info',
            content: message,
            datetime: yengin.getTime()
        });
        self.show();
    };

    self.error = function (message) {
        priv._data.push({
            type: 'error',
            content: message,
            datetime: yengin.getTime()
        });
        self.show();
    };

    self.show = function () {
        if(priv._target !== null) {
            /* Clean up - FIFO */
            while(priv._data.length > priv._limit) {
                priv._data.splice(0,1);
            }
            /* Show it */
            priv._target.html('');
            yengin.each(priv._data, function (item) {
                priv._target.addChild(
                    yengin.getNewObj('p').html({
                        info: item.content,
                        error: '/!\\ '+item.content
                    }[item.type])
                );
            });
        }
    };

    self.attach = function (selector) {
        if(yengin.isset(selector)) {
            priv._target = yengin(selector);
        }
    };

    /* Setters */

    self.setLimit = function (limit) {
        if(yengin.istype(limit, 'Number')) {
            priv._limit = limit;
        }
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Logs'};
    return (priv.init(selector), self);
}
