import yengin from 'yengin';
import Zun from './zun';

export default function Camera () {
    var self = {}, priv = {};
    priv.enableRenderModes = ['3D','XY','YZ','XZ'];

    /* Constructor */

    priv.init = function () {
        /* Default Properties */
        priv._dist = 1;
        priv._scene = null;
        priv._canvas = null;
        priv._vector = new Zun.Vector();
        priv._renderMode = priv.enableRenderModes[0];
        priv._asyncBool = false;
    };

    /* Public Methods */

    self.render = function (scene) {
        /* 3D renders */
        if(priv._renderMode == '3D')
            priv.mode3D(scene);
        /* 2D renders (async mode) */
        if(yengin.isIn(priv._renderMode, ['XY','YZ','XZ']))
            priv.asyncMode2D(priv._renderMode);
    };

    /* Rotate the camera on XYZ */
    self.rotate = function (x, y, z) {
        return self.rotateX(x).rotateY(y).rotateZ(z);
    };

    /* Rotate the camera on the axis X */
    self.rotateX = function (angle) {
        return (priv._vector.rotateX(angle), self);
    };

    /* Rotate the camera on the axis Y */
    self.rotateY = function (angle) {
        return (priv._vector.rotateY(angle), self);
    };

    /* Rotate the camera on the axis Z */
    self.rotateZ = function (angle) {
        return (priv._vector.rotateZ(angle), self);
    };

    /* Move the camera from the current point (relative location) */
    self.move = function (x, y, z) {
        return (priv._vector.move(x, y, z), self);
    };

    /* Move the camera to a specific location */
    self.moveTo = function (x, y, z) {
        return (priv._vector.moveTo(x, y, z), self);
    };

    /* Enable the mode : Auto Resize */
    self.autoResize = function () {
        yengin(window).on('resize', function () {
            priv._canvas.dpp('auto','auto');
            self.render();
        });
        return self;
    };

    /* Enable the mode : Scroll Zoom */
    self.scrollZoom = function () {
        var evt = function (e) {
            priv._dist += (e.deltaY > 0 ? 0.5 : -0.5);
            self.render();
        };
        priv._canvas.on('mousewheel', evt);
        priv._canvas.on('wheel', evt);
        return self;
    };

    /* Render Modes */

    priv.mode3D = function () {
        var matrix, pt, ptCenter, zBuffer, zMin, layers = [];
        priv._canvas.clear();
        /* Prepare */
        var params = {};
        params.screen = priv._canvas.getDpp(),
        params.location = priv._vector.getLocation(),
        params.direction = priv._vector.getDirection(),
        params.fov = (params.screen.x + params.screen.y)/2;
        params.minScreen = Math.min(params.screen.x, params.screen.y)/2;
        /* Clear the cache */
        priv._cache.clear();
        /* Loop: Shapes */
        priv._scene.eachShapes(function (shape) {
            /* Loop: Edges (-> without faces) */
            shape.eachEdges(function (edge) {
                (matrix = [], ptCenter = {x:0, y:0, k:edge.getPoints().length||1}, zBuffer = 0, zMin = null);
                /* Loop: Points (-> projection) */
                edge.eachPoints(function (point) {
                    pt = priv.projection3D(point, params);
                    matrix.push([pt.x, pt.y]);
                    ptCenter.x += pt.x;
                    ptCenter.y += pt.y;
                    zBuffer += pt.z;
                    zMin = (zMin !== null ? Math.min(zMin, pt.z) : pt.z);
                });
                /* Draw a polygon */
                layers.push({
                    'type': 'edge',
                    'data': matrix,
                    'color': 'rgba(0,0,0,0)',
                    'border': edge.getWeight(), 'borderColor': edge.getMaterial().toRgba(),
                    'mode': 1,
                    'center': {
                        'x': ptCenter.x/ptCenter.k,
                        'y': ptCenter.y/ptCenter.k
                    },
                    'z': zMin,
                    'zc': zBuffer/ptCenter.k
                });
            });
            /* Loop: Faces (-> future polygons) */
            shape.eachFaces(function (face) {
                (matrix = [], ptCenter = {x:0, y:0, k:face.getPoints().length||1}, zBuffer = 0, zMin = null);
                /* Loop: Points (-> projection) */
                face.eachPoints(function (point) {
                    pt = priv.projection3D(point, params);
                    matrix.push([pt.x, pt.y]);
                    ptCenter.x += pt.x;
                    ptCenter.y += pt.y;
                    zBuffer += pt.z;
                    zMin = (zMin !== null ? Math.min(zMin, pt.z) : pt.z);
                });
                /* Draw a polygon */
                layers.push({
                    'type': 'face',
                    'data': matrix,
                    'color': face.getMaterial().toRgba(),
                    'border': 1, 'borderColor': 'rgba(0,0,0,0)',
                    'mode': 1,
                    'center': {
                        'x': ptCenter.x/ptCenter.k,
                        'y': ptCenter.y/ptCenter.k
                    },
                    'z': zMin,
                    'zc': zBuffer/ptCenter.k
                });
            });
        });
        /* Loop: Particles */
        priv._scene.eachParticles(function (particle) {
            pt = priv.projection3D(particle.getPoint(), params);
            /* Draw a circle */
            layers.push({
                'type': 'particle',
                'x': pt.x, 'y': pt.y,
                'radius': params.minScreen * particle.getScale() * Math.pow(0.5, pt.z-1),
                'border': 1, 'borderColor': 'rgba(0,0,0,0.1)',
                'color': particle.getMaterial().toRgba(),
                'mode': 1,
                'center': { 'x': pt.x, 'y': pt.y },
                'z': pt.z, 'zc': pt.z
            });
        });
        /* Sort by zBuffer */
        layers = yengin.quickSortArray(layers, function (a, b) {
            return (a.z == b.z ? (
                a.zc == b.zc ? 0 : (a.zc > b.zc ? -1 : 1)
            ) : (a.z > b.z ? -1 : 1));
        });
        /* Show the layers */
        yengin.each(layers, function (layer) {
            if(layer.z >= 0) {
                if(layer.type == 'particle') {
                    priv._canvas.drawCircle(layer);
                } else {
                    priv._canvas.drawPolygon(layer);
                }
                // priv._canvas.drawText({
                //     'text': Math.floor(layer.z*100)/100,
                //     'x': layer.center.x, 'y': layer.center.y,
                //     'color': '#000', 'font': 'normal 12pt Calibri'
                // });
            }
        });
    };

    priv.mode2D = function (view) {
        var matrix;
        priv._canvas.clear();
        /* Prepare */
        var params = {};
        params.screen = priv._canvas.getDpp();
        params.fov = (params.screen.x+params.screen.y)/2;
        params.view = view;
        /* Loop: Shapes */
        priv._scene.eachShapes(function (shape) {
            /* Loop: Edges */
            shape.eachEdges(function (edge) {
                matrix = [];
                /* Loop: Points */
                edge.eachPoints(function (point) {
                    matrix.push(priv.projection2D(point, params));
                });
                /* Draw a polygon */
                priv._canvas.drawPolygon({
                    'data': matrix,
                    'color': 'rgba(0,0,0,0)',
                    'border': 1, 'borderColor': edge.getMaterial().toRgba(),
                    'mode': 1
                });
            });
            /* Loop: Faces */
            shape.eachFaces(function (face) {
                matrix = [];
                /* Loop: Points */
                face.eachPoints(function (point) {
                    matrix.push(priv.projection2D(point, params));
                });
                /* Draw a polygon */
                priv._canvas.drawPolygon({
                    'data': matrix,
                    'color': face.getMaterial().toRgba(),
                    'border': 1, 'borderColor': 'rgba(0,0,0,0.3)',
                    'mode': 1
                });
            });
        });
        /* Loop: Particles */
        // priv._scene.eachParticles(function (particle) {
        //     matrix = priv.projection2D(particle.getPoint(), params);
        //     /* Draw a circle */
        //     priv._canvas.drawCircle({
        //         'x': matrix[0], 'y': matrix[1],
        //         'radius': particle.getScale(),
        //         'border': 1, 'borderColor': 'rgba(0,0,0,0.1)',
        //         'color': particle.getMaterial().toRgba(),
        //         'mode': 1
        //     });
        // });
    };

    priv.asyncMode2D = function (view) {
        yengin.wait(function () {
            if(!priv._asyncBool) {
                priv._asyncBool = true;
                priv.mode2D(view);
                priv._asyncBool = false;
            }
        }, 0);
    };

    priv.projection3D = function (point, params) {
        var pt, factor, result;
        /* Cache */
        if(priv._cache.has(point)) {
            return priv._cache.get(point);
        }
        /* Isolation */
        pt = point.get();
        /* Apply: Camera.Translation */
        pt = pt.sub(params.location.get());
        /* Apply: Camera.Direction */
        pt = pt.rotateX(params.direction[0]);
        pt = pt.rotateY(params.direction[1]);
        pt = pt.rotateZ(params.direction[2]);
        /* Calculate factor */
        pt._j = (priv._dist+pt._j)||1;
        factor = params.fov/pt._j;
        /* Add the projection */
        result = {
            'x': pt._i * factor + params.screen.x/2,
            'y': pt._k * -factor + params.screen.y/2,
            'z': Math.sqrt(pt._i*pt._i + pt._j*pt._j + pt._k*pt._k)
                * (pt._j >= 0 ? 1 : -1)
        };
        /* Insert in the cache */
        priv._cache.set(point, result);
        return result;
    };

    priv.projection2D = function (point, params) {
        var pt, ps, factor;
        /* Isolation */
        pt = point.get();
        /* Select the positions */
        switch(params.view) {
            case 'XY':
                ps = [pt._i, pt._j, pt._k];
                break;
            case 'YZ':
                ps = [pt._j, pt._k, pt._i];
                break;
            case 'XZ':
                ps = [pt._i, pt._k, pt._j];
                break;
            default:
                ps = [0, 0, 0];
                break;
        }
        /* Calculate the factor */
        factor = Math.max(params.screen.x,params.screen.y)/(priv._dist||1);
        /* Add the point */
        return [
            ps[0] * factor + (params.screen.x/2),
            params.screen.y - (ps[1] * factor + (params.screen.y/2))
        ];
    };

    /* Cache */

    priv._cache = {
        data: {},
        stats: 0,
        /* Functions */
        has: function (point) {
            var index = this.getIndex(point);
            return yengin.isset(this.data[index]);
        },

        get: function (point) {
            var index = this.getIndex(point);
            if(yengin.isset(this.data[index])) {
                this.stats++;
                return this.data[index];
            } return null;
        },

        getIndex: function (point) {
            point = point.get();
            return [point._i, point._j, point._k].join(';');
        },

        set: function (point, result) {
            var index = this.getIndex(point);
            this.data[index] = result;
            return self;
        },

        clear: function () {
            return (this.data = {}, this.stats = 0, self);
        },

        debug: function () {
            return (1/(Object.keys(this.data).length+this.stats)) * this.stats;
        }
    };

    /* Getters */

    self.getRenderMode = function () {
        return priv._renderMode;
    };

    self.getCanvas = function () {
        return priv._canvas;
    };

    self.getDist = function () {
        return priv._dist;
    };

    self.getVector = function () {
        return priv._vector;
    };

    /* Setters */

    self.setRenderMode = function (mode) {
        if(priv.enableRenderModes.indexOf(mode) >= 0) {
            priv._renderMode = mode;
        } return self;
    };

    self.setScene = function (scene) {
        if(scene.constructor.name == 'Zun.Scene') {
            priv._scene = scene;
        } return self;
    };

    self.setCanvas = function (selector) {
        yengin(selector).exist(function () {
            priv._canvas = yengin.canvas(selector);
            priv._canvas.dpp('auto','auto');
        }); return self;
    };

    self.setDist = function (dist) {
        if(yengin.isset(dist) && yengin.istype(dist, 'Number')) {
            priv._dist = dist;
        } return self;
    };

    self.setVector = function (vector) {
        if(vector.constructor.name == 'Zun.Vector') {
            priv._vector = vector;
        } return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Camera'};
    return (priv.init(), self);
}
