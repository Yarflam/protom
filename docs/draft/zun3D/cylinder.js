import Zun from './zun';

export default function Cylinder (args) {
    var self = {}, priv = {},
        tau = Math.PI*2;

    /* Constructor */

    priv.init = function (args) {
        var obj; args = args||{};
        args.n = args.n>>0||8;
        /* Construct */
        var i, pt, fTop=[], fBot=[], points=[], edges=[], faces=[];
        for(i=0; i < args.n; i++) {
            /* Point */
            pt = [
                Math.cos( tau / args.n * i ) * 0.5,
                Math.sin( tau / args.n * i ) * 0.5
            ];
            /* Circle top and bottom */
            points.push([ pt[0], pt[1], 0.5 ]);
            points.push([ pt[0], pt[1], -0.5 ]);
            /* Jointure */
            edges.push([ i * 2, i * 2 + 1 ]);
            edges.push([ i * 2, (i < args.n-1 ? i+1 : 0) * 2 ]);
            edges.push([ i * 2 + 1, (i < args.n-1 ? i+1 : 0) * 2 + 1 ]);
            /* Faces */
            faces.push([
                i * 2, i * 2 + 1,
                (i < args.n-1 ? i+1 : 0) * 2 + 1,
                (i < args.n-1 ? i+1 : 0) * 2
            ]);
            fTop.push(i*2);
            fBot.push(i*2+1);
        }
        faces.push(fTop);
        faces.push(fBot);
        /* Build */
        obj = new Zun.Shape(points, edges, faces);
        obj.legacy(self).setPivot([0, 0, 0]);
        return obj;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Cylinder'};
    return priv.init(args);
}
