import yengin from 'yengin';
import Zun from './zun';

export default function Edge (points) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (points) {
        /* Default Properties */
        priv._scale = 1;
        priv._weight = 1;
        priv._material = new Zun.Material({ rgba: [0,0,0,0.3] });

        /* Matrix objects */
        priv._points = [];
        priv._vector = new Zun.Vector();

        /* Points */
        if(yengin.isset(points))
            self.setPoints(points);
    };

    /* Public Methods */

    /* Rotate the edge on XYZ */
    self.rotate = function (x, y, z) {
        return self.rotateX(x).rotateY(y).rotateZ(z);
    };

    /* Rotate the edge on the axis X */
    self.rotateX = function (angle) {
        priv._vector.rotateX(angle);
        Zun.Transform.rotateX(priv._points, angle);
        return self;
    };

    /* Rotate the edge on the axis Y */
    self.rotateY = function (angle) {
        priv._vector.rotateY(angle);
        Zun.Transform.rotateY(priv._points, angle);
        return self;
    };

    /* Rotate the edge on the axis Z */
    self.rotateZ = function (angle) {
        priv._vector.rotateZ(angle);
        Zun.Transform.rotateZ(priv._points, angle);
        return self;
    };

    /* Move the edge from the current point (relative location) */
    self.move = function (x, y, z) {
        priv._vector.move(x, y, z);
        Zun.Transform.move(priv._points, x, y, z);
        return self;
    };

    /* Move the edge to a specific location */
    self.moveTo = function (x, y, z) {
        priv._vector.moveTo(x, y, z);
        Zun.Transform.moveTo(priv._points, x, y, z);
        return self;
    };

    /* Change the scale */
    self.resize = function (ratio) {
        if(yengin.isset(ratio) && yengin.istype(ratio, 'Number')) {
            Zun.Transform.resize(priv._points, ratio);
            priv._scale *= ratio;
        }
    };

    /* Get the edge's central point (average) */
    self.getPivot = function () {
        return Zun.Transform.getPivot(priv._points);
    };

    /* Each points */
    self.eachPoints = function (callback) {
        yengin.each(priv._points, callback);
    };

    /* Getters */

    self.getScale = function () {
        return priv._scale;
    };

    self.getWeight = function () {
        return priv._weight;
    };

    self.getVector = function () {
        return priv._vector;
    };

    self.getPoints = function () {
        return priv._points;
    };

    self.getMaterial = function () {
        return priv._material;
    };

    /* Setters */

    self.setWeight = function (weight) {
        return (priv._weight = weight>>0||1, self);
    };

    self.setVector = function (vector) {
        if(vector.constructor.name == 'Zun.Vector')
            priv._vector = vector;
        return self;
    };

    self.setPoints = function (points) {
        if(yengin.istype(points, 'Array') && points.length == 2)
            priv._points = points;
        return self;
    };

    self.setMaterial = function (material) {
        if(yengin.isset(material) && material.constructor.name == 'Zun.Material')
            priv._material = material;
        return self;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Edge'};
    return (priv.init(points), self);
}
