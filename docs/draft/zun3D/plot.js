import Zun from './zun';

export default function Plot (args) {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function (args) {
        args = args||{};
        args.x = args.x||[-1,1];
        args.y = args.y||[-1,1];
        args.z = args.z||[-1,1];
        args.fit = (args.fit||20)+1;
        args.fct = args.fct||function(x,y){return(Math.sin(x)*Math.cos(y));};
        args.fctColors = args.fctColors||function(){return([0,0,0]);};

        /* Create the shape */
        var obj = new Zun.Shape();
        obj.legacy(self);

        /* Define the pivot */
        obj.getPivot();

        /* Calculate the plot */
        var i, x, y, z, c, fx, fy, fz, edge, points=[], edges=[];
        args.fitSquare = args.fit * args.fit;
        for(i=0; i < args.fitSquare; i++) {
            /* Get X & Y */
            x = i % args.fit;
            y = Math.floor(i / args.fit);
            /* Plot function */
            fx = ((args.x[1]-args.x[0])/(args.fit-1)) * x + args.x[0];
            fy = ((args.y[1]-args.y[0])/(args.fit-1)) * y + args.y[0];
            fz = args.fct(fx, fy);
            fz = (!isNaN(fz) ? fz : 0);
            /* Add points */
            z = (fz-args.z[0])/((args.z[1]-args.z[0])/(args.fit-1));
            fx = (1/(args.fit-1)) * x - 0.5;
            fy = (1/(args.fit-1)) * y - 0.5;
            fz = (1/(args.fit-1)) * z - 0.5;
            points.push(new Zun.Point(fx, fy, (fz < -0.5 ? -0.5 : (fz > 0.5 ? 0.5 : fz))));
            /* Add an edge */
            if(x > 0) {
                edge = new Zun.Edge([ points[x-1 + y * args.fit], points[i] ]);
                c = Zun.Transform.getPivot(edge.getPoints()).get();
                edge.getMaterial().setColors(args.fctColors(c._i, c._j, c._k));
                edges.push(edge);
            }
            if(y > 0) {
                edge = new Zun.Edge([ points[x + (y-1) * args.fit], points[i] ]);
                c = Zun.Transform.getPivot(edge.getPoints()).get();
                edge.getMaterial().setColors(args.fctColors(c._i, c._j, c._k));
                edges.push(edge);
            }
        }

        /* Assign the points and edges */
        obj.setEdges(points, edges);

        /* Return the shape */
        return obj;
    };

    /* Signature */
    self.constructor = {'name': 'Zun.Plot'};
    return priv.init(args);
}
