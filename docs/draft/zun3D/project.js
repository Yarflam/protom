export default function Project () {
    var self = {}, priv = {};

    /* Constructor */

    priv.init = function () {};

    /* Signature */
    self.constructor = {'name': 'Zun.Project'};
    return (priv.init(), self);
}
