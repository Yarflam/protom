const path = require('path');

module.exports = {
    mode: 'production',
    entry: './core/web.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'protom.min.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/
            }
        ]
    }
};
