# ![logo](https://gitlab.com/Yarflam/protom/-/raw/master/public/images/logo32x32.png) Protom

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)
![version](https://img.shields.io/badge/app-v1.0.0-blue.svg)

## Abstract

Protom is mathematical base to work with modern concept such like Machine Learning, Deep Learning, Quantum machine, 3D render or Data Analyze. The goal is to teach one language for many applications with an identical process. Behind your jobs, Protom can optimize the algorithms with the same solutions: balance between CPU / GPU / RAM, compression and standardization of data, easy import / export and compatibility.

The core of Protom is developed in Javascript. You can use it in your web and/or desktop applications. The Gitlab deposit is open source, you can fork or contribute. I will ask you to create units tests and comment your code.

## Prerequisites

You should have to install Node.js on your computer (Windows / Linux / Mac / ARM systems).

[Download here](https://nodejs.org/en/download/)

## Install

You can clone the project and install the node modules:

```bash
git clone https://gitlab.com/Yarflam/protom.git &&\
cd protom &&\
npm install
```

## How to use it?

I write some papers in the `/docs/papers` folder:

-   [Protom Development](https://gitlab.com/Yarflam/protom/-/blob/master/docs/papers/DevelopmentPaper.ipynb): see all features, structures and data types.
-   [Quantum programming](https://gitlab.com/Yarflam/protom/-/blob/master/docs/papers/Quantum%20Programming.ipynb): an overview of quantum mechanics to work with qubits

## Explore

### Tree view

-   `core`
    -   `bayes`
        -   `Bayes.js`: first test to apply Bayes' theorem
    -   `helpers`
        -   `gentools.js`: access to random tools
            -   _Lack of clarity -> refactoring is required_
    -   `quantum`
        -   `Qubit`: simple quantum unit.
        -   `Qureg`: Quantum Register (_aka Q-System or Q-Space_) - manipulate entranglement, quantum gates and measure.
    -   `struct`
        -   `controllers`
            -   `Activation.js`: activation functions
                -   _{ Machine Learning }_
            -   `Caller.js`
                -   **Role**: call a specific function in data type classes
                -   _Example: convert to another type_
            -   `Operation.js`
                -   **Role**: perform a mathematical operation
            -   `Optimizer.js`: use loss functions to minimize the errors
                -   _Example: Gradient descent_
                -   _{ Machine Learning }_
        -   `datatypes`
            -   `Complex.js`: define a complex number
                -   _Example: `4 + 2i`_
            -   `Matrix.js`: define a matrix
                -   **Notice**: division is not supported
                -   _Example: `[ 2, 5, 11 ]`_
            -   `Num.js`: define a number
                -   _Example: `133.7`_
            -   `Quaternion.js`: define a quaternion
                -   _Example: `20 + 21i + 7j + 8k`_
            -   `Tensor.js`: define a tensor
                -   _Example: `[ <vect1>, <vect2> ... <vectN> ]`_
            -   `Vector.js`: define a vector
                -   _Example: `< 3.1, 4.15, 1, 0, 1 >`_
        -   `nodes`
            -   `Const`: constants
            -   `Placeholder`: dynamic values
            -   `Var`: variables
    -   `Protom.js`: main application.
        -   **Role**: manage the structures and orchestrates the operations
    -   `index.js` & `web.js`: different entry points (Node.js or web front)
-   `docs`
    -   `draft`
        -   `zun3D`: 3D engine
    -   `papers`
        -   `DevelopmentPaper.ipynb`: general
        -   `Quantum Programming.ipynb`: quantum mechanics application
-   `public`: front pages (web)
-   `sandbox`:
    -   `dev-playground.js`: my tests on the Protom core (Yarflam)
    -   `search-bayes`: Test in application of Bayes' theorem
    -   `search-map.js` & `search-map2.js`: generate maps in command line (just for fun)
-   `test`: automated tests with Jest
    -   `protom-func.spec.js`: check the Protom core
    -   `struct-datatypes.spec.js`: check the data types (num, complex, quaternion, vector, matrix, tensor)
    -   `struct-nodes.spec.js`: check the nodes (const, var, placeholder)

### Data types

| Type       | About                             |                Refers to                 |        Example         |
| :--------- | :-------------------------------- | :--------------------------------------: | :--------------------: |
| Num        | Simple number                     |                                          |         `2.75`         |
| Complex    | Complex number (real + imaginary) |                   Num                    |        `4 + 2i`        |
| Quaternion | Quaternion (w, i, j, k)           |                   Num                    |  `20 + 21i + 7j + 8k`  |
| Vector     | One dimension D                   |         Num, Complex, Quaternion         |     `<0.73, 0.12>`     |
| Matrix     | Two dimensions DxD                |         Num, Complex, Quaternion         | `[[ 1, 2 ], [ 3, 4 ]]` |
| Tensor     | Multiple dimensions DxDx...D      | Num, Complex, Quaternion, Vector, Matrix | `[[ 1, 2 ], [ 3, 4 ]]` |

### Nodes

| Node        | About                                                |
| :---------- | :--------------------------------------------------- |
| Const       | Constant value                                       |
| Var         | Dynamic value, it can evolve by optimizer algorithms |
| Placeholder | Free space to have fill by inputs                    |

### Controllers

| Controller | About                          | Elements                                                                                                                                                                                                                                    |
| :--------- | :----------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Activation | Activation function            | <ul><li>Gauss</li><li>Heaviside</li><li>Leaky-Relu</li><li>ReLU</li><li>Sigmoid / Logistic</li><li>Softmax [N.I.]</li><li>Softplus [N.I.]</li><li>Softsign [N.I.]</li><li>Swish</li><li>Tanh</li></ul>                                      |
| Optimizer  | Algorithm to optimize learning | <ul><li>Adadelta</li><li>Adagrad</li><li>Adam</li><li>Adamax</li><li>Ftrl</li><li>Gradient Descent</li><li>Momentum</li><li>Proximal Adagrad</li><li>Proximal Gradient Descent</li><li>RMS Prop</li><li>SDCA</li><li>SyncReplicas</li></ul> |
| Loss       | Loss function                  |                                                                                                                                                                                                                                             |
| Caller     | Call a specific method of node |                                                                                                                                                                                                                                             |
| Operation  | Calculate node in the graph    |                                                                                                                                                                                                                                             |

## Engines

### Summary

| Name           | About                            |
| :------------- | :------------------------------- |
| Neural Network | Multilayer perceptron            |
| Quantum System | Simulation of a quantum computer |
| 3D render      | Orthogonal projections           |
| Data tools     | Statistics                       |

### Quantum System

In development...

#### Standards

Sequence of Q-bytes:

All gates are written in big-endian - an easy-to-use common representation - but they are executed in little-endian.

Compatibility with numbers:

You can use Number (or _Num_ object) and Complex number.

#### Gates

| Gates                            | Code  | Params | Matrix                                         |
| :------------------------------- | :---: | :----: | :--------------------------------------------- |
| PauliX <small>/ SigmaX</small>   |   X   |   1    | `[[ 0, 1 ], [ 1, 0 ]]`                         |
| PauliY <small>/ SigmaY</small>   |   Y   |   1    | `[[ 0, -i ], [ i, 0 ]]`                        |
| PauliZ <small>/ SigmaZ</small>   |   Z   |   1    | `[[ 1, 0 ], [ 0, -1 ]]`                        |
| Hadamard                         |   H   |   1    | `1/sqrt(2) * [[ 1, 1 ], [ 1, -1 ]]`            |
| SqrtNot                          |       |   1    | `[[ (1+i)/2, (1-i)/2 ], [ (1-i)/2, (1+i)/2 ]]` |
| Phase                            |   P   | 1 + θ  | `[[ 1, 0 ], [ 0, e^(θi) ]]`                    |
| S-Gate                           |   S   |   1    | `[[ 1, 0 ], [ 0, e^(π/2i) ]]`                  |
| T-Gate                           |   T   |   1    | `[[ 1, 0 ], [ 0, e^(π/4i) ]]`                  |
| I-Gate <small>/ Identity</small> |   I   |   1    | `[[ 1, 0 ], [ 0, 1 ]]`                         |
| CNot                             |  CN   |   2    | `see below`                                    |
| CtrlZ                            |  CZ   |   2    | `see below`                                    |
| Swap                             |  SW   |   2    | `see below`                                    |
| SqrtSwap                         |  RS   |   2    | `see below`                                    |
| Toffoli                          |  CCX  |   3    | `see below`                                    |
| Fredkin                          | CSWAP |   3    | `see below`                                    |

**CNot**

```
[[ 1, 0, 0, 0 ],
 [ 0, 1, 0, 0 ],
 [ 0, 0, 0, 1 ],
 [ 0, 0, 1, 0 ]]
```

**CtrlZ**

```
[[ 1, 0, 0,  0 ],
 [ 0, 1, 0,  0 ],
 [ 0, 0, 1,  0 ],
 [ 0, 0, 0, -1 ]]
```

**Swap**

```
[[ 1, 0, 0, 0 ],
 [ 0, 0, 1, 0 ],
 [ 0, 1, 0, 0 ],
 [ 0, 0, 0, 1 ]]
```

**SqrtSwap**

```
[[ 1,       0,       0, 0 ],
 [ 0, (1+i)/2, (1-i)/2, 0 ],
 [ 0, (1-i)/2, (1+i)/2, 0 ],
 [ 0,       0,       0, 1 ]]
```

**Toffoli**

```
[[ 1, 0, 0, 0, 0, 0, 0, 0 ],
 [ 0, 1, 0, 0, 0, 0, 0, 0 ],
 [ 0, 0, 1, 0, 0, 0, 0, 0 ],
 [ 0, 0, 0, 1, 0, 0, 0, 0 ],
 [ 0, 0, 0, 0, 1, 0, 0, 0 ],
 [ 0, 0, 0, 0, 0, 1, 0, 0 ],
 [ 0, 0, 0, 0, 0, 0, 0, 1 ],
 [ 0, 0, 0, 0, 0, 0, 1, 0 ]]
```

**Fredkin**

```
[[ 1, 0, 0, 0, 0, 0, 0, 0 ],
 [ 0, 1, 0, 0, 0, 0, 0, 0 ],
 [ 0, 0, 1, 0, 0, 0, 0, 0 ],
 [ 0, 0, 0, 1, 0, 0, 0, 0 ],
 [ 0, 0, 0, 0, 1, 0, 0, 0 ],
 [ 0, 0, 0, 0, 0, 0, 1, 0 ],
 [ 0, 0, 0, 0, 0, 1, 0, 0 ],
 [ 0, 0, 0, 0, 0, 0, 0, 1 ]]
```

## Authors

-   Yarflam - _initial work_

## License

The project is licensed under Creative Commons (BY-NC-SA) - see the [LICENSE.md](https://gitlab.com/Yarflam/protom/-/blob/master/LICENSE.md) file for details.
