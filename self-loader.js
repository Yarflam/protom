const Module = require('module');

/* List of modules imported */
const _store = {};

/* Proxy component */
function ProxyComponent() {
    let proxy,
        count = 1,
        target = { default: {} };
    /* Anonymous function */
    function Anonymous(...args) {
        return new target.default(...args);
    }
    /* Proxy */
    proxy = new Proxy(Anonymous, {
        get: function(obj, prop) {
            console.log('prop', prop);
            if (prop == '_add') {
                count++;
            }
            if (prop == '_set') {
                return item => {
                    count--;
                    if (!count) {
                        target.default = item;
                    }
                    return proxy;
                };
            }
            if (typeof prop == 'symbol') {
                const symbol = prop.toString();
                console.log(symbol);
                return proxy;
            }
            return proxy;
        },
        // @args: (obj, _, args)
        apply: function() {
            return target.default;
        }
    });
    return proxy;
}

/* Node.js Loader - rewrite */
const originalLoader = Module._load;
// @args: (request, parent)
Module._load = function(request) {
    /* Counter */
    if (_store[request] === undefined) {
        _store[request] = ProxyComponent();
    } else {
        _store[request]._add;
    }
    /* Call */
    return _store[request]._set(originalLoader.apply(this, arguments));
};
