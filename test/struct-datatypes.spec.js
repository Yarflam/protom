const { Struct } = require('../core');
const { Num, Complex, Quaternion, Matrix } = Struct; // datatypes

// const serialize = x => JSON.stringify(x);

describe('Struct.Datatypes.Num', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const x = 3;
        const num = new Num(x);
        expect(num._value).toEqual(x);
        expect(num.toString()).toEqual(String(x));
        expect(Object.getOwnPropertyNames(Num.prototype)).toIncludeSameMembers(
            (
                'constructor,toString,__ope,add,sub,mul,div,pow,mod,pow2,inv,' +
                'abs,sqrt,cos,sin,tan,log,AND,OR,NOT,NOR,NAND,XOR,XNOR,isNotZero,' +
                'convert,toComplex,toQuaternion,toVector,toTensor,toMatrix,' +
                'getValue,setValue'
            ).split(',')
        );
    });

    /*
     *   Operations
     */
    it('add', () => {
        const x = [2, 3];
        const a = new Num(x[0]);
        const b = new Num(x[1]);
        expect(a.add(b)._value).toEqual(x[0] + x[1]);
    });

    it('sub', () => {
        const x = [2, 3];
        const a = new Num(x[0]);
        const b = new Num(x[1]);
        expect(a.sub(b)._value).toEqual(x[0] - x[1]);
    });

    it('mul', () => {
        const x = [2, 3];
        const a = new Num(x[0]);
        const b = new Num(x[1]);
        expect(a.mul(b)._value).toEqual(x[0] * x[1]);
    });

    it('div', () => {
        const x = [4, 2];
        const a = new Num(x[0]);
        const b = new Num(x[1]);
        expect(a.div(b)._value).toEqual(x[0] / x[1]);
    });

    it('pow', () => {
        const x = [4, 3];
        const num = new Num(x[0]);
        expect(num.pow(x[1])._value).toEqual(Math.pow(x[0], x[1]));
    });

    it('mod', () => {
        const x = [4, 3];
        const num = new Num(x[0]);
        expect(num.mod(x[1])._value).toEqual(x[0] % x[1]);
    });

    it('pow2', () => {
        const x = 4;
        const num = new Num(x);
        expect(num.pow2()._value).toEqual(x * x);
    });

    it('inv', () => {
        const x = 4;
        const num = new Num(x);
        expect(num.inv()._value).toEqual(-x);
    });

    /*
     *   Math
     */
    it('abs', () => {
        const x = -1;
        const num = new Num(x);
        expect(num.abs(x)._value).toEqual(Math.abs(x));
    });

    it('sqrt', () => {
        const x = 4;
        const num = new Num(x);
        expect(num.sqrt(x)._value).toEqual(Math.sqrt(x));
    });

    it('cos', () => {
        const x = 1.2;
        const num = new Num(x);
        expect(num.cos()._value).toEqual(Math.cos(x));
    });

    it('sin', () => {
        const x = 1.2;
        const num = new Num(x);
        expect(num.sin()._value).toEqual(Math.sin(x));
    });

    it('tan', () => {
        const x = 1.2;
        const num = new Num(x);
        expect(num.tan()._value).toEqual(Math.tan(x));
    });

    it('log', () => {
        const x = 1.2;
        const num = new Num(x);
        expect(num.log()._value).toEqual(Math.log(x));
    });

    /*
     *   Conditions
     */
    it('isNotZero', () => {
        expect(new Num(0).isNotZero()).toBeFalsy();
        expect(new Num(1).isNotZero()).toBeTruthy();
    });

    /*
     *   Getters & Setters
     */
    it('getValue / setValue', () => {
        const x = 20;
        const num = new Num();
        num.setValue(x);
        expect(num.getValue()).toEqual(x);
    });
});

describe('Struct.Datatypes.Complex', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const num = new Complex(2, -3);
        expect(num.getReal()._value).toEqual(2);
        expect(num.getImaginary()._value).toEqual(-3);
        expect(num.toString()).toEqual('2-3i');
        expect(
            Object.getOwnPropertyNames(Complex.prototype)
        ).toIncludeSameMembers(
            (
                'constructor,toString,__ope,add,sub,mul,div,pow,norm,pow2,inv,' +
                'abs,sqrt,isNotZero,convert,toQuaternion,toVector,toTensor,toMatrix,' +
                'getValue,getReal,getImaginary,setReal,setImaginary,setValue'
            ).split(',')
        );
    });

    /*
     *   Operations
     */
    it('add', () => {
        const x = [[1, 2], [2, 1]];
        const a = new Complex(...x[0]);
        const b = new Complex(...x[1]);
        const check = [x[0][0] + x[1][0], x[0][1] + x[1][1]];
        const result = a.add(b);
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    it('sub', () => {
        const x = [[8, 5], [4, 2]];
        const a = new Complex(...x[0]);
        const b = new Complex(...x[1]);
        const check = [x[0][0] - x[1][0], x[0][1] - x[1][1]];
        const result = a.sub(b);
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    it('mul', () => {
        const x = [[8, 5], [4, 2]];
        const a = new Complex(...x[0]);
        const b = new Complex(...x[1]);
        const check = [
            x[0][0] * x[1][0] - x[0][1] * x[1][1],
            x[0][0] * x[1][1] + x[0][1] * x[1][0]
        ];
        const result = a.mul(b);
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    it('div', () => {
        const x = [[12, 9], [3, 3]];
        const a = new Complex(...x[0]);
        const b = new Complex(...x[1]);
        const check = [
            (x[0][0] * x[1][0] + x[0][1] * x[1][1]) /
                (x[1][0] * x[1][0] + x[1][1] * x[1][1]),
            (x[0][1] * x[1][0] - x[0][0] * x[1][1]) /
                (x[1][0] * x[1][0] + x[1][1] * x[1][1])
        ];
        const result = a.div(b);
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    it('pow', () => {
        const x = [2, 5];
        const num = new Complex(...x);
        const result = num.pow(3);
        let ck = [x[0] * x[0] - x[1] * x[1], x[0] * x[1] + x[1] * x[0]];
        ck = [ck[0] * ck[0] - ck[1] * ck[1], ck[0] * ck[1] + ck[1] * ck[0]];
        expect(result.getReal()._value).toEqual(ck[0]);
        expect(result.getImaginary()._value).toEqual(ck[1]);
    });

    it('norm', () => {
        const x = [2, 3];
        const num = new Complex(...x);
        const result = num.norm();
        expect(result.getValue()).toEqual(Math.sqrt(x[0] * x[0] + x[1] * x[1]));
    });

    it('pow2', () => {
        const x = [2, 5];
        const num = new Complex(...x);
        const check = [x[0] * x[0] - x[1] * x[1], x[0] * x[1] + x[1] * x[0]];
        const result = num.pow2();
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    it('inv', () => {
        const x = [2, 5];
        const num = new Complex(...x);
        const check = [-x[0], -x[1]];
        const result = num.inv();
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    /* Math */
    it('abs', () => {
        const x = [-2, -3];
        const num = new Complex(...x);
        const check = [Math.abs(x[0]), Math.abs(x[1])];
        const result = num.inv();
        expect(result.getReal()._value).toEqual(check[0]);
        expect(result.getImaginary()._value).toEqual(check[1]);
    });

    it('sqrt', () => {
        const x = [1, 1];
        const num = new Complex(...x);
        expect(num.sqrt().getValue()).toEqual(Math.sqrt(2));
    });

    /* Conditions */
    it('isNotZero', () => {
        expect(new Complex(0, 0).isNotZero()).toBeFalsy();
        expect(new Complex(0, 1).isNotZero()).toBeFalsy();
        expect(new Complex(1, 0).isNotZero()).toBeFalsy();
        expect(new Complex(1, 1).isNotZero()).toBeTruthy();
    });

    /*
     *   Getters & Setters
     */
    it('getValue / setValue', () => {
        const x = [2, 3];
        const num = new Complex();
        num.setValue(...x);
        expect(num.getReal()._value).toEqual(x[0]);
        expect(num.getImaginary()._value).toEqual(x[1]);
    });

    it('getReal / setReal', () => {
        const x = 2;
        const num = new Complex();
        num.setReal(x);
        expect(num.getReal()._value).toEqual(x);
    });

    it('getImaginary / setImaginary', () => {
        const x = 3;
        const num = new Complex();
        num.setImaginary(x);
        expect(num.getImaginary()._value).toEqual(x);
    });
});

describe('Struct.Datatypes.Quaternion', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const x = [2, -3, 7, 1];
        const num = new Quaternion(...x);
        const value = num.getValue();
        new Array(4)
            .fill(0)
            .map((_, i) => expect(value[i]._value).toEqual(x[i]));
        expect(num.toString()).toEqual('2-3i+7j+1k');
        expect(
            Object.getOwnPropertyNames(Quaternion.prototype)
        ).toIncludeSameMembers(
            (
                'constructor,toString,__ope,add,sub,mul,div,norm,abs,neg,conjugate,' +
                'normalize,isNotZero,convert,toVector,toTensor,toMatrix,getValue,' +
                'getXYZ,setW,setIJK,setXYZ,setValue'
            ).split(',')
        );
    });

    /*
     *   Operations
     */
    it('add', () => {
        const x = [[6, 1, 2, 3], [6, 3, 2, 1]];
        const a = new Quaternion(...x[0]);
        const b = new Quaternion(...x[1]);
        const check = new Array(4).fill(0).map((_, i) => x[0][i] + x[1][i]);
        const result = a.add(b).getValue();
        new Array(4)
            .fill(0)
            .map((_, i) => expect(result[i]._value).toEqual(check[i]));
    });

    it('sub', () => {
        const x = [[6, 8, 5, 2], [6, 4, 3, 1]];
        const a = new Quaternion(...x[0]);
        const b = new Quaternion(...x[1]);
        const check = new Array(4).fill(0).map((_, i) => x[0][i] - x[1][i]);
        const result = a.sub(b).getValue();
        new Array(4)
            .fill(0)
            .map((_, i) => expect(result[i]._value).toEqual(check[i]));
    });

    it('mul', () => {});

    it('div', () => {});

    it('norm', () => {});

    /* Math */
    it('abs', () => {});

    it('neg', () => {});

    it('conjugate', () => {});

    it('normalize', () => {});

    /* Conditions */
    it('isNotZero', () => {});

    /*
     *   Getters & Setters
     */
    it('getValue / setValue', () => {});

    it('getXYZ / setXYZ / setIJK', () => {});
});

describe('Struct.Datatypes.Matrix', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const x = [[2, 3]];
        const mtx = new Matrix(x);
        expect(mtx._value.toString()).toEqual(x.toString());
        expect(mtx.toString()).toEqual(`[\n  [ ${x[0][0]}, ${x[0][1]} ]\n]`);
    });

    /*
     *   Operations
     */
    it('add', () => {
        const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        const a = new Matrix(x);
        expect(a.add(a)._value).toEqual(
            new Matrix([[2, 4, 6], [8, 10, 12], [14, 16, 18]])._value
        );
    });

    it('substract', () => {
        const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        const a = new Matrix(x);
        expect(a.sub(a)._value).toEqual(
            new Matrix([[0, 0, 0], [0, 0, 0], [0, 0, 0]])._value
        );
    });

    it('multiply', () => {
        const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        const a = new Matrix(x);
        expect(a.mul(a)._value).toEqual(
            new Matrix([[30, 36, 42], [66, 81, 96], [102, 126, 150]])._value
        );
    });

    // it('add a number', () => {
    //     const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    //     const a = new Matrix(x);
    //     const b = new Num(2);
    //     expect(a.add(b)._value).toEqual(
    //         new Matrix([[3, 4, 5], [6, 7, 8], [9, 10, 11]])._value
    //     );
    // });
    //
    // it('substract a number', () => {
    //     const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    //     const a = new Matrix(x);
    //     const b = new Num(2);
    //     expect(a.sub(b)._value).toEqual(
    //         new Matrix([[-1, 0, 1], [2, 3, 4], [5, 6, 7]])._value
    //     );
    // });
    //
    // it('multiply by number', () => {
    //     const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    //     const a = new Matrix(x);
    //     const b = new Num(2);
    //     expect(a.mul(b)._value).toEqual(
    //         new Matrix([[2, 4, 6], [8, 10, 12], [14, 16, 18]])._value
    //     );
    // });
    //
    // it('divide by number', () => {
    //     const x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    //     const a = new Matrix(x);
    //     const b = new Num(2);
    //     expect(a.div(b)._value).toEqual(
    //         new Matrix([[0.5, 1, 1.5], [2, 2.5, 3], [3.5, 4, 4.5]])._value
    //     );
    // });

    /*
     *   Getters & Setters
     */
    it('getValue / setValue', () => {
        const x = [[1, 2, 3]];
        const mtx = new Matrix(x);
        expect(mtx._value.toString()).toEqual(x.toString());
    });

    it('getShape <refactoring>', () => {
        console.log('getShape <refactoring>');
        // const x = [[1, 2, 3], [3, 4, 5], [6, 7, 8]];
        // const mtx = new Matrix(x);
        // expect(serialize(mtx.getShape()._value)).toEqual(serialize([3, 3]));
    });
});
