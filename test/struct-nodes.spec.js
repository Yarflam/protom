const { Protom, Struct } = require('../core');
const { Operation } = Struct; // controllers
const { Num, Complex, Matrix } = Struct; // datatypes
const { Const, Placeholder, Var } = Struct; // nodes

/* Protom Environment */
const app = new Protom();

describe('Struct.Nodes.Const', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const myConst = new Const(app, 42);
        expect(myConst._value).toBeInstanceOf(Num);
        expect(myConst._type).toEqual('Num');
    });

    /*
     *   Getters
     */
    it('getValue', () => {
        const myConst = new Struct.Const(app, 42);
        expect(myConst.getValue()).toBeInstanceOf(Num);
        expect(myConst.getValue()._value).toEqual(42);
    });

    it('getName', () => {
        const name = 'const_1';
        const myConst = new Struct.Const(app, 42, name);
        expect(myConst.getName()).toEqual(name);
    });

    /*
     *   Operations
     */
    it('add', () => {
        const myConstA = new Struct.Const(app, 42);
        const myConstB = new Struct.Const(app, 42);
        const calc = myConstA.add(myConstB);
        expect(calc).toBeInstanceOf(Operation);
        expect(calc._ope).toEqual('add');
        expect(calc._a).toEqual(myConstA);
        expect(calc._b).toEqual(myConstB);
    });

    it('sub', () => {
        const myConstA = new Struct.Const(app, 42);
        const myConstB = new Struct.Const(app, 42);
        const calc = myConstA.sub(myConstB);
        expect(calc).toBeInstanceOf(Operation);
        expect(calc._ope).toEqual('sub');
        expect(calc._a).toEqual(myConstA);
        expect(calc._b).toEqual(myConstB);
    });

    it('mul', () => {
        const myConstA = new Struct.Const(app, 42);
        const myConstB = new Struct.Const(app, 42);
        const calc = myConstA.mul(myConstB);
        expect(calc).toBeInstanceOf(Operation);
        expect(calc._ope).toEqual('mul');
        expect(calc._a).toEqual(myConstA);
        expect(calc._b).toEqual(myConstB);
    });

    it('div', () => {
        const myConstA = new Struct.Const(app, 42);
        const myConstB = new Struct.Const(app, 42);
        const calc = myConstA.div(myConstB);
        expect(calc).toBeInstanceOf(Operation);
        expect(calc._ope).toEqual('div');
        expect(calc._a).toEqual(myConstA);
        expect(calc._b).toEqual(myConstB);
    });

    /*
     *   Type 'auto' detection
     */
    it('complex number 1', () => {
        const myConst = new Struct.Const(app, 'i');
        expect(myConst.getValue()).toBeInstanceOf(Complex);
        expect(
            myConst
                .getValue()
                .getReal()
                .getValue()
        ).toEqual(0);
        expect(
            myConst
                .getValue()
                .getImaginary()
                .getValue()
        ).toEqual(1);
    });

    it('complex number 2', () => {
        const myConst = new Struct.Const(app, [1, 2]);
        expect(myConst.getValue()).toBeInstanceOf(Complex);
        expect(
            myConst
                .getValue()
                .getReal()
                .getValue()
        ).toEqual(1);
        expect(
            myConst
                .getValue()
                .getImaginary()
                .getValue()
        ).toEqual(2);
    });

    it('complex number 3', () => {
        const myConst = new Struct.Const(app, [3], false, app.Complex);
        expect(myConst.getValue()).toBeInstanceOf(Complex);
        expect(
            myConst
                .getValue()
                .getReal()
                .getValue()
        ).toEqual(3);
        expect(
            myConst
                .getValue()
                .getImaginary()
                .getValue()
        ).toEqual(0);
    });

    it('matrix 1', () => {
        const values = [[1, 2, 3, 4, 5]];
        const myConst = new Struct.Const(app, values);
        expect(myConst.getValue()).toBeInstanceOf(Matrix);
        expect(myConst.getValue()._value.toString()).toEqual(values.toString());
    });

    it('matrix 2', () => {
        const values = [[1, 2]];
        const myConst = new Struct.Const(app, values, '', 'Matrix');
        expect(myConst.getValue()).toBeInstanceOf(Matrix);
        expect(myConst.getValue()._value.toString()).toEqual(values.toString());
    });

    it('math pi', () => {
        const myConst = new Struct.Const(app, 'pi');
        expect(myConst.getValue()).toBeInstanceOf(Num);
        expect(myConst.getValue()._value).toEqual(Math.PI);
    });

    it('math e', () => {
        const myConst = new Struct.Const(app, 'e');
        expect(myConst.getValue()).toBeInstanceOf(Num);
        expect(myConst.getValue()._value).toEqual(Math.E);
    });
});

describe('Struct.Nodes.Placeholder', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const name = 'ph_1';
        const myPh = new Placeholder(app, name);
        expect(myPh.getName()).toEqual(name);
    });
});

describe('Struct.Nodes.Var', () => {
    /*
     *   Main work
     */
    it('instance', () => {
        const name = 'var_1';
        const myVar = new Var(app, 0, name);
        expect(myVar.getName()).toEqual(name);
        expect(myVar.getValue()).toBeInstanceOf(Num);
        expect(myVar.getValue()._value).toEqual(0);
    });

    /*
     *   Setters
     */
    it('setValue 1', () => {
        const myVar = new Var(app, 0);
        myVar.setValue(10);
        expect(myVar.getValue()._value).toEqual(10);
    });

    it('setValue 2', () => {
        const myVar = new Var(app, [1, 2]);
        myVar.setValue(10);
        expect(myVar.getValue()).toBeInstanceOf(Complex);
        expect(
            myVar
                .getValue()
                .getReal()
                .getValue()
        ).toEqual(10);
        expect(
            myVar
                .getValue()
                .getImaginary()
                .getValue()
        ).toEqual(0);
    });
});
