const { Protom, Struct } = require('../core');
const { Operation } = Struct; // controllers
const { Num, Matrix } = Struct; // datatypes
const { Const, Placeholder, Var } = Struct; // nodes

/* First context (initial environment) */
const firstContext = app => {
    return {
        x: app.placeholder('x'),
        a: app.var(2, 'a'),
        b: app.const(3, 'b')
    };
};

describe('Protom.Functional', () => {
    it('define variables', () => {
        const app = new Protom();
        const { x, a, b } = firstContext(app);
        expect(x).toBeInstanceOf(Placeholder);
        expect(a).toBeInstanceOf(Var);
        expect(b).toBeInstanceOf(Const);
    });

    it('constants', () => {
        const app = new Protom();
        const test = [
            'Version',
            'Authors',
            'None',
            'Num',
            'Complex',
            'Vector',
            'Matrix',
            'Tensor'
        ];
        expect(
            test.filter(key => typeof app[key] !== 'undefined').length
        ).toEqual(test.length);
    });

    it('define the graph', () => {
        const app = new Protom();
        const { x, a, b } = firstContext(app);
        const y = x.mul(a).add(b);
        /* Top operation */
        expect(y).toBeInstanceOf(Operation);
        expect(y._ope).toEqual('add');
        expect(y._a).toBeInstanceOf(Operation);
        expect(y._b).toBeInstanceOf(Const);
        /* Left operation */
        expect(y._a._ope).toEqual('mul');
        expect(y._a._a).toBeInstanceOf(Placeholder);
        expect(y._a._b).toBeInstanceOf(Var);
    });

    it('evaluate the graph', () => {
        const app = new Protom();
        const { x, a, b } = firstContext(app);
        const y = x.mul(a).add(b); // 2x+3
        /* Evaluate */
        const input = 3;
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(2 * input + 3);
    });

    it('evaluate the graph in two times', () => {
        const app = new Protom();
        const { x, a, b } = firstContext(app);
        const y = x.mul(a).add(b); // 2x+3
        const z = y.add(app.var(10)); // y+10
        /* Evaluate */
        const input = 4;
        const check = 2 * input + 3;
        const results = z.eval({ x: input }, y);
        expect(results).toBeInstanceOf(Array);
        expect(results.length).toEqual(2);
        expect(results[0]).toBeInstanceOf(Num);
        expect(results[1]).toBeInstanceOf(Num);
        expect(results[0].getValue()).toEqual(check + 10);
        expect(results[1].getValue()).toEqual(check);
    });

    it('evaluate the product of two matrix', () => {
        const app = new Protom();
        const x = app.placeholder('x', app.Matrix);
        const b = app.const(
            [[10, 11, 12], [13, 14, 15], [16, 17, 18]],
            'b',
            app.Matrix
        );
        const y = x.mul(b);
        /* Evaluate */
        const input = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
        const results = y.eval({ x: input });
        expect(results.getValue()).toEqual(
            new Matrix([
                [84, 90, 96],
                [201, 216, 231],
                [318, 342, 366]
            ]).getValue()
        );
    });

    // it('evaluate the product between a number and a matrix', () => {
    //     const app = new Protom();
    //     const x = app.placeholder('x', app.Num);
    //     const b = app.const([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 'b', app.Matrix);
    //     const y = x.mul(b);
    //     /* Evalute */
    //     const input = 5;
    //     const results = y.eval({ x: input });
    //     expect(results.getValue()).toEqual(
    //         new Matrix([[5, 10, 15], [20, 25, 30], [35, 40, 45]]).getValue()
    //     );
    // });

    it('evalute the sum of two matrix', () => {
        const app = new Protom();
        const x = app.placeholder('x', app.Matrix);
        const b = app.const([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 'b', app.Matrix);
        const y = x.add(b);
        /* Evalute */
        const input = [[9, 8, 7], [6, 5, 4], [3, 2, 1]];
        const results = y.eval({ x: input });
        expect(results.getValue()).toEqual(
            new Matrix([[10, 10, 10], [10, 10, 10], [10, 10, 10]]).getValue()
        );
    });

    // it('evalute the sum of one tensor with a number', () => {
    //     const app = new Protom();
    //     const x = app.placeholder('x', app.Number);
    //     const b = app.const([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    //     const y = x.add(b);
    //     /* Evalute */
    //     const input = 1;
    //     const results = y.eval({ x: input });
    //     expect(results.getValue()).toEqual(
    //         new Matrix([[2, 3, 4], [5, 6, 7], [8, 9, 10]]).getValue()
    //     );
    // });

    it('evalute the substract of two matrix', () => {
        const app = new Protom();
        const x = app.placeholder('x', app.Matrix);
        const b = app.const([[1, 2, 3], [4, 5, 6], [7, 8, 9]], 'b', app.Matrix);
        const y = x.sub(b);
        /* Evalute */
        const input = [[9, 8, 7], [6, 5, 4], [3, 2, 1]];
        const results = y.eval({ x: input });
        expect(results.getValue()).toEqual(
            new Matrix([[8, 6, 4], [2, 0, -2], [-4, -6, -8]]).getValue()
        );
    });

    // it('evalute the substract of one tensor with a number', () => {
    //     const app = new Protom();
    //     const x = app.placeholder('x', app.Number);
    //     const b = app.const([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    //     const y = x.sub(b);
    //     /* Evalute */
    //     const input = 1;
    //     const results = y.eval({ x: input });
    //     expect(results.getValue()).toEqual(
    //         new Matrix([[0, 1, 2], [3, 4, 5], [6, 7, 8]]).getValue()
    //     );
    // });

    it('act. fct gauss', () => {
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.gauss(x);
        /* Evaluate */
        const input = 7;
        const check = Math.pow(Math.E, -(input * input) / 2);
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(check);
    });

    it('act. fct heaviside', () => {
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.heaviside(x);
        /* Evaluate */
        const input = 8;
        const check = input >= 0 ? 1 : 0;
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(check);
    });

    it('act. fct leaky-relu', () => {
        let input, result;
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.leakyRelu(x);
        /* Evaluate */
        (input = 3), (result = y.eval({ x: input }));
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(3);
        (input = -3), (result = y.eval({ x: input }));
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(-0.03);
    });

    it('act. fct relu', () => {
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.relu(x);
        /* Evaluate */
        const input = 9;
        const check = Math.max(0, input);
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(check);
    });

    it('act. fct sigmoid', () => {
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.sigmoid(x);
        /* Evaluate */
        const input = 5;
        const check = 1 / (1 + Math.pow(Math.E, -input));
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(check);
    });

    it('act. fct swish', () => {
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.swish(x);
        /* Evaluate */
        const input = 10;
        const check = input / (1 + Math.pow(Math.E, -input));
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(check);
    });

    it('act. fct tanh', () => {
        const app = new Protom();
        const x = app.placeholder('x');
        const y = app.tanh(x);
        /* Evaluate */
        const input = 6;
        const check = 2 / (1 + Math.pow(Math.E, -2 * input)) - 1;
        const result = y.eval({ x: input });
        expect(result).toBeInstanceOf(Num);
        expect(result.getValue()).toEqual(check);
    });
});
