# Changelog

## [DRAFT] - 2020-01-01
### Details
- Tensor can be use.
- Protom.isNode(obj<Object>) has added: check if the object is a node.

## [DRAFT] - 2019-12-15
### Details
- Tensor has split to build three classes: Vector (one dimension), Matrix (two dimensions), Tensor (one array that contains many complex objects).
- The Tensor definition is not completely finish.
- The quaternions has been added.
- Draft versions are the sandboxes of implementation and refactoring.
- Dev versions are the little stories of the project.
- After we will see the alpha, beta, gamma and production versions in the Master branch. The versions will can be imported in your projects.